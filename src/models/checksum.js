async function checksum (value) {
    const encoder = new TextEncoder();
    const data = (typeof value === 'string') ? encoder.encode(value) : (typeof value === 'object') ? encoder.encode(JSON.stringify(value)) : encoder.encode(typeof value);
    const hashBuffer = await crypto.subtle.digest('SHA-256', data);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join('');
    return 'SHA-256:' + hashHex;

};

export default checksum;
