import Dexie from 'dexie';
import { nanoid } from 'nanoid';
import checksum from './checksum';
import { encryptAESGCMChain } from './crypto';

export class EchoesDB extends Dexie {
    // flag: 0, normal; others delete.
    constructor() {
        super('EchoesDB');
        this.version(1).stores({//echo(es): editors,categories,hybrids,options(,execute/establishments,synch)
            editors: '&nanoid, title, flag', //, option // option: for complex usage, cryptography...
            categories: '&nanoid, editor, key, [editor+key], parent, tag, [editor+tag], title, type, mimetype, flag', // TreeView: diary(key) used year-month(day) category by default.//add parent // add type for split hybrid data and index.// mimetype: application/draft-js-raw+json
            hybrids: '&nanoid, sum', // , data, option //category, key, title, flag, 
            options: 'key, value, flag', // value: use nanoid or string value.//add flag
        });
    }
}

export const echo = new EchoesDB();

export async function updateCategoryMimetype(categoryNanoid, mimetype) {
    await echo.categories.where({nanoid: categoryNanoid}).modify({mimetype: mimetype}).then(
        count => console.log(Date.now() + ': update category mimetype count=' + count)
    );
};

export async function updateCategoryTitle(categoryNanoid, title) {
    let count = 0;
    await echo.categories.where({nanoid: categoryNanoid}).modify({title: title}).then(
        (v) => {
            count = v;
        }
    );
    return count;
};

export async function updateEditorTitle(editorNanoid, title) {
    let count = 0;
    await echo.editors.where({nanoid: editorNanoid}).modify({title: title}).then(
        (v) => {
            count = v;
        }
    );
    return count;
};

export async function updateEditorOption(editorNanoid, option) {
    let count = 0;
    await echo.editors.where({nanoid: editorNanoid}).modify({option: option}).then(
        (v) => {
            count = v;
        }
    );
    return count;
};

export async function encryptHybrid(hybridNanoid, option) {
    await echo.hybrids.where({nanoid: hybridNanoid}).modify({option: option}).then(
        count => console.log(Date.now() + ': encryptHybrid records count=' + count)
    );
};

export async function updateHybrid(hybridNanoid, raw, master, option) {
    await checksum(raw).then(async (value) => {
        if (master) {
            const chain = master.ctu ? master : await encryptAESGCMChain(master, raw);
            await echo.hybrids.where({nanoid: hybridNanoid}).modify({data: {crypto: {ctu: chain.ctu}}, sum: value, option: {...option, crypto: {eku: chain.eku}}}).then(
                count => console.log(Date.now() + ': update encrypted records count=' + count)
            );
        }
        else {
        //echo.hybrids.where({nanoid: hybridNanoid}).modify({data: raw, format: format, sum: value}).then(
            await echo.hybrids.where({nanoid: hybridNanoid}).modify({data: raw, sum: value}).then(
                count => console.log(Date.now() + ': update records count=' + count)
            );
        }
        await echo.options.where({key: 'hybrid'}).modify({value: hybridNanoid}).then(
            count => console.log(Date.now() + ': update options count=' + count)
        );
    });
};

export async function fetchCategoriesArray(editorNanoid) {
    const categories = await echo.categories.where({editor: editorNanoid}).toArray();
    return categories;
}

export async function fetchHybridCategory(hybridNanoid) {
    let hybrid;
    await echo.categories.where({nanoid: hybridNanoid}).first().then(
        value => hybrid = {...value}
    );
    await echo.hybrids.where({nanoid: hybridNanoid}).first().then(
        value => hybrid = {...hybrid, ...value}
    );
    return hybrid;//await echo.hybrid.where({nanoid: nanoid}).toArray();
};

async function init() {
    const options = [];
    const editor = {nanoid: nanoid(), title: nanoid(10), flag: 0, option: {}};
    options.push({key: 'editor', value: editor.nanoid, flag: 0});
    await echo.editors.add(editor);
    const today = new Date();
    const year = today.getFullYear().toString();
    const month = (today.getMonth() + 1).toString().padStart(2, '0');
    const date = today.getDate().toString().padStart(2, '0');
    let ty = year.padStart(6, '0');
    let td = ty + month + date, tm = ty + month
    let category = {nanoid: nanoid(), editor: editor.nanoid, key: 'journal', title: 'Journal', type: 'root', flag: 0};//, , key, , title, flag
    await echo.categories.add(category);
    category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, tag: ty, title: year, type: 'category', flag: 0};
    await echo.categories.add(category);
    category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, tag: tm, title: month, type: 'category', flag: 0};
    await echo.categories.add(category);
    category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, tag: td, title: date, type: 'hybrid', flag: 0};
    await echo.categories.add(category);
    const hybrid = {nanoid: category.nanoid};
    options.push({key: 'hybrid', value: hybrid.nanoid, flag: 0});
    await echo.hybrids.add(hybrid);
    category = {nanoid: nanoid(), editor: editor.nanoid, title: 'Editor', type: 'root', flag: 0};
    await echo.categories.add(category);
    options.push({key: 'theme', value: 'light', flag: 0});
    await echo.options.bulkAdd(options);
    // editors: '&nanoid, title, flag', //, option // option: for complex usage, cryptography...
    // categories: '&nanoid, editor, key, [editor+key], parent, tag, [editor+tag], title, type, mimetype, flag', // TreeView: diary(key) used year-month(day) category by default.//add parent // add type for split hybrid data and index.// mimetype: application/draft-js-raw+json
    // hybrids: '&nanoid, sum', // , data, option //category, key, title, flag, 
    // options: 'key, value, flag', // value: use nanoid or string value.//add flag

};

export async function addCategory(c) {
    let category, hybrid;// = {nanoid: nanoid(), editor: editor.nanoid, key: 'journal', title: 'Journal', type: 'root', flag: 0};//, , key, , title, flag
    await echo.categories.where({nanoid: c.parent}).first().then(
        async (e) => {
            if (!e) {
                console.log('Lose parent=' + c.parent);

            }
            else {
                category = {nanoid: nanoid(), editor: e.editor, title: c.title, type: c.type, flag: 0};
                if (c.type !== 'root') {
                    category.parent = c.parent;
                }
                if (c.type === 'hybrid') {
                    hybrid = {nanoid: category.nanoid};
                }
            }
        }
    );
    if (category) {
        console.log('Add categories=' + JSON.stringify(category));
        await echo.categories.add(category);
    }
    if (hybrid) {
        console.log('Add hybrid=' + JSON.stringify(hybrid));
        await echo.hybrids.add(hybrid);
    }
    return category;
};

export async function addDiary(editorID) {
    const today = new Date();
    const year = today.getFullYear().toString();
    const month = (today.getMonth() + 1).toString().padStart(2, '0');
    const date = today.getDate().toString().padStart(2, '0');
    let ty = year.padStart(6, '0');
    let td = ty + month + date, tm = ty + month;
    let categories = [], category, hybrid;// = {nanoid: nanoid(), editor: editor.nanoid, key: 'journal', title: 'Journal', type: 'root', flag: 0};//, , key, , title, flag
    await echo.categories.where({editor: editorID, tag: td}).count().then(
        async (d) => {
            if (!d) {
                console.log('tag.count=' + d);
                await echo.categories.where({editor: editorID, tag: tm}).first().then(
                    async (m) => {
                        if (!m) {
                            console.log('tag.count=' + m);
                            await echo.categories.where({editor: editorID, tag: ty}).first().then(
                                async (y) => {
                                    if (!y) {
                                        await echo.categories.where({editor: editorID, key: 'journal'}).first().then(
                                            async (j) => {
                                                if (j) {
                                                    category = {nanoid: nanoid(), editor: editorID, parent: j.nanoid, tag: ty, title: year, type: 'category', flag: 0};
                                                    categories.push(category);
                                                }
                                                else {
                                                    console.error('editor=' + editorID + ' lost journal');
                                                }
                                            }
                                        );
                                        console.log('tag.count=' + y);
                                    }
                                    else {
                                        category = {nanoid: y.nanoid};
                                        console.log('tag.count=' + JSON.stringify(y));
                                    }
                                }
                            );
                            category = {nanoid: nanoid(), editor: editorID, parent: category.nanoid, tag: tm, title: month, type: 'category', flag: 0};
                            categories.push(category);
                        }
                        else {
                            category = {nanoid: m.nanoid};
                            //console.log('tag.count=' + JSON.stringify(m));
                        }
                    }
                );
                category = {nanoid: nanoid(), editor: editorID, parent: category.nanoid, tag: td, title: date, type: 'hybrid', flag: 0};
                hybrid = {nanoid: category.nanoid};
                categories.push(category);

            }
        }
    );
    if (categories.length) {
        console.log('today.categories=' + JSON.stringify(categories));
        await echo.categories.bulkAdd(categories);
    }
    if (hybrid) {
        console.log('today.hybrid=' + JSON.stringify(hybrid));
        await echo.hybrids.add(hybrid);
    }
    // category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, tag: ty, title: year, type: 'category', flag: 0};
    // await echo.categories.add(category);
    // category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, tag: tm, title: month, type: 'category', flag: 0};
    // await echo.categories.add(category);
    // category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, tag: td, title: date, type: 'hybrid', flag: 0};
};

echo.on('populate', init);

echo.on('ready', async () => {
    if (window.location.hostname === 'areditors.opentiss.uk') {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = async (event) => {
          // In local files, status is 0 upon success in Mozilla Firefox
          if (xhr.readyState === XMLHttpRequest.DONE) {
            const status = xhr.status;
            if (status === 200) {
              // The request has been completed successfully
              const psi = await echo.options.where({key: 'psi'}).first();
              if (!psi) {
                await echo.options.add({key: 'psi', value: xhr.responseText.trim(), flag: 0});
              }
            } else {
              // Oh no! There has been an error with the request!
            }
          }
        };
        xhr.open('GET', '/favicon.txt', true);
        xhr.send();
    }
});

// test code...
//Dexie.delete('Echo');
export class AreditorsDB extends Dexie {
    constructor() {
        super('AreditorsDB');
        this.version(1).stores({//echo(es): editors,categories,hybrids,options(,execute/establishments,synch)
            config: '++id, key, value',
            editors: '++id, nanoid, key, flag, option',
            raws: '++id, editor, category, key, title, tag, sum, flag, raw', // Primary key and indexed props
        });
        this.version(2).stores({//echo(es): editors,categories,hybrids,options(,execute/establishments,synch)
            config: '++id, key, value',
            editors: '++id, nanoid, key, flag, option',
            raws: '++id, editor, category, key, title, tag, sum, flag, raw', // Primary key and indexed props
            categories: '++id, nanoid, editor, key, title, flag', // TreeView: diary(key) used year-month(day) category by default.
            hybrids: '++id, nanoid, editor, category, key, title, flag, option, data, format, tag, sum',
            options: '++id, key, value', // value: use nanoid or string value.
            //
        });
    }
}

//export const db = new AreditorsDB();

export class Echo extends Dexie {
    constructor() {
        super('Echo');
        this.version(1).stores({//echo(es): editors,categories,hybrids,options(,execute/establishments,synch)
            editors: '++id, nanoid, key, flag, option',
            categories: '++id, nanoid, editor, key, title, flag', // TreeView: diary(key) used year-month(day) category by default.//add parent
            hybrids: '++id, nanoid, editor, category, key, title, flag, option, data, format, tag, sum',
            options: '++id, key, value', // value: use nanoid or string value.
            //
        });
        this.version(2).stores({//echo(es): editors,categories,hybrids,options(,execute/establishments,synch)
            editors: '++id, nanoid, key, flag, option',
            categories: '++id, nanoid, editor, key, parent, title, flag', // TreeView: diary(key) used year-month(day) category by default.//add parent
            hybrids: '++id, nanoid, editor, category, key, title, flag, option, data, format, tag, sum',
            options: '++id, key, value, flag', // value: use nanoid or string value.//add flag
            //
        });
        this.version(3).stores({//echo(es): editors,categories,hybrids,options(,execute/establishments,synch)
            editors: '++id, nanoid, key, flag, option',
            categories: '++id, nanoid, editor, key, parent, title, type, flag', // TreeView: diary(key) used year-month(day) category by default.//add parent // add type for split hybrid data and index.
            hybrids: '++id, nanoid, option, data, format, tag, sum', //category, key, title, flag, 
            options: '++id, key, value, flag', // value: use nanoid or string value.//add flag
            //
        });
    }
}

// let isMigration = true;
// const db = null;//new EchoesDB();

/*if (!isMigration) {
    db.delete().then(() => {//Dexie.delete('EchoesDB')
        console.log("Database successfully deleted");
    }).catch((err) => {
        console.error(err + ": Could not delete database");
    }).finally(() => {
        console.error("Database deleted finally.");
    });
    
}*/

export async function initEcho(editorID) {
    const options = [];
    const editor = {nanoid: editorID ? editorID : nanoid(), key: nanoid(10), flag: 0, option: {}};
    if (!editorID) {
        options.push({key: 'editor', value: editor.nanoid, flag: 0});
        await echo.editors.add(editor);    
    }
    const today = new Date();
    const year = today.getFullYear().toString();
    const month = (today.getMonth() + 1).toString().padStart(2, 0);
    const date = today.getDate().toString().padStart(2, 0);
    let category = {nanoid: nanoid(), editor: editor.nanoid, key: 'journal', title: 'Journal', type: 'root', flag: 0};//, , key, , title, flag
    await echo.categories.add(category);
    category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, title: year, type: 'category', flag: 0};
    await echo.categories.add(category);
    category = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, title: month, type: 'category', flag: 0};
    await echo.categories.add(category);
    const hybrid = {nanoid: nanoid(), editor: editor.nanoid, parent: category.nanoid, title: date, type: 'hybrid', flag: 0};//nanoid, editor, category, key, title, flag, option, data, format, tag, sum
    if (!editorID) {
        options.push({key: 'hybrid', value: hybrid.nanoid, flag: 0});
    }
    await echo.categories.add(hybrid);
    await echo.hybrids.add({nanoid: hybrid.nanoid});
    category = {nanoid: nanoid(), editor: editor.nanoid, title: 'Editor', type: 'root', flag: 0};
    await echo.categories.add(category);
    if (!editorID) {
        options.push({key: 'theme', value: 'light', flag: 0});
    }
    await echo.options.bulkAdd(options);
};

// echo.on('ready_', async () => {
//     // Add lost categories.
//     const options = await echo.options.toArray();
//     let editorID;
//     options.forEach(element => {
//         if (element.key === 'editor') {
//             editorID = element.value;
//         }
//         //else {
//         //    console.log(element.key);
//         //}
//     });
//     if (editorID) {
//         await echo.categories.count(async (value) => {
//             if (value === 0) {
//                 await echo.categories.clear();
//                 initEcho(editorID);
//                 /*options.forEach(element => {
//                     if (element.key === 'editor') {
//                         console.log(JSON.stringify(element));
//                         initEcho(element.value);
//                     }
//                     else {
//                         console.log(element.key);
    
//                     }
//                 });*/
//             }
//             console.log('categories count=' + value);
//         });
//         // Add today.
//         const today = new Date();
//         const year = today.getFullYear().toString();
//         const month = (today.getMonth() + 1).toString().padStart(2, 0);
//         const date = today.getDate().toString().padStart(2, 0);
//         //let categoryJournal;
//         let categoryMonth;
//         let category;
//         /*await echo.categories.where({editor: editorID, key: 'journal', type: 'root'}).first().then(
//             async (j) => {
//                 if (j) {
//                     await echo.categories.where({parent: j.nanoid, title: year, type: 'category'}).first().then(
//                         async (y) => {
//                             if (y) {
//                                 await echo.categories.where({parent: y.nanoid, title: month, type: 'category'}).first().then(
//                                     async (m) => {
//                                         if (m) {
//                                             //console.log(JSON.stringify(m));
//                                             categoryMonth = m.nanoid;
//                                             await echo.categories.where({parent: m.nanoid, title: date, type: 'hybrid'}).first().then(
//                                                 async (d) => {
//                                                     if (d) {
//                                                         console.log(JSON.stringify(d));
//                                                     }
//                                                     else {
//                                                         category = {nanoid: nanoid(), editor: editorID, parent: m.nanoid, title: date, type: 'hybrid', flag: 0};
//                                                         await echo.categories.add(category);
//                                                         const hybrid = {nanoid: category.nanoid};
//                                                         await echo.hybrids.add(hybrid);
//                                                         console.log(date);
//                                                     }

//                                                 }
//                                             );

//                                         }
//                                     }
//                                 );
    
//                             }
//                         }
//                     );

//                 }
//             }
//         );*/
//         // Temp: fix hybrid and category.
//         await echo.hybrids.count().then(c => console.log('hybrids=' + c));
//         /*await echo.categories.where({editor: editorID, type: 'hybrid'}).each(
//             async (c) => {
//                 console.log(c.nanoid);
//                 await echo.hybrids.where({nanoid: c.nanoid}).count().then(
//                     (h) => console.log(c.nanoid + '=' + h)
//                     async (h) => {
//                         console.log(c.nanoid + '=' + h);
//                         //if (!h) {
//                         //    const hybrid = {nanoid: c.nanoid};
//                         //    console.log(JSON.stringify(hybrid))
//                         //    await echo.hybrids.add(hybrid);
//                         //}
//                     }
//                 );
//             }
//         );*/
//         /*await echo.hybrids.toArray(
//             async (h) => {
//                 h.forEach(
//                     async (e) => {
//                         //console.log(JSON.stringify(e));// e.nanoid
//                         await echo.categories.where({editor: editorID, nanoid: e.nanoid}).count().then(
//                             async (c) => {
//                                 if (!c) {
//                                     console.log(categoryMonth + ':' + e.nanoid + '@' + e.category + '=' + c);
//                                     if (!e.category) {
//                                         await echo.categories.where({editor: editorID, title: 'Editor', type: 'root'}).first().then(
//                                             async (a) => {
//                                                 console.log(JSON.stringify(a));
//                                                 //const hybrid = {nanoid: e.nanoid, editor: editorID, parent: a.nanoid, title: 'hybrid', type: 'hybrid', flag: 0};
//                                                 //await echo.categories.add(hybrid);
                                            
//                                             }
//                                         );
//                                     }
//                                     else {
//                                         //const hybrid = {nanoid: e.nanoid, editor: editorID, parent: categoryMonth, title: e.title, type: 'hybrid', flag: 0};
//                                         //await echo.categories.add(hybrid);
//                                     }
//                                     //console.log(JSON.stringify(h));

//                                 }
//                             }
//                             //async (h) => {
//                                 //console.log(c.nanoid + '=' + h);
//                                 //if (!h) {
//                                 //    const hybrid = {nanoid: c.nanoid};
//                                 //    console.log(JSON.stringify(hybrid))
//                                 //    await echo.hybrids.add(hybrid);
//                                 //}
//                             //}
//                         );
        
//                     }
//                 );
//                 //console.log(h.nanoid);
//             }
//         );*/
    
//     }
//     // ONLY for data migration.
//     //editors: '&nanoid, title, flag, option',// option: for complex usage, cryptography...
//     //categories: '&nanoid, editor, key, parent, tag, title, type, format, flag', // TreeView: diary(key) used year-month(day) category by default.//add parent // add type for split hybrid data and index.
//     //hybrids: '&nanoid, data, option, sum', //category, key, title, flag, 
//     //options: 'key, value, flag', // value: use nanoid or string value.//add flag

//     /*if (isMigration && db) {// === null
//         db.open();
//         await db.options.count(async (options) => {
//             console.log('options.count=' + options);
//             if (!options) {
//                 await echo.editors.count(async (count) => {
//                     if (count) {
//                         console.log('editors.count=' + count);
//                         await echo.editors.each(
//                             async (value) => {
//                                 let e = {nanoid: value.nanoid};
//                                 if (value.key) {
//                                     e.title = value.key;
//                                 }
//                                 if (value.flag) {
//                                     e.flag = value.flag;
//                                 }
//                                 if (value.option) {
//                                     e.option = value.option;
//                                 }
//                                 await db.editors.put(e);
//                             }
//                         );

//                     }
//                 });
//                 await echo.categories.count(async (count) => {
//                     console.log('categories.count=' + count);
//                     if (count) {
//                         const categories = await echo.categories.toArray();
//                         const hybrids = await echo.hybrids.toArray();
//                         await echo.categories.each(
//                             async (value) => {
//                                 console.log(JSON.stringify(value));
//                                 let c;
//                                 if (value.parent) {
//                                     let tag;
//                                     let p = categories.find( category => category.nanoid === value.parent );
//                                     if (p) {
//                                         if (p.parent) {
//                                             let pp = categories.find( category => category.nanoid === p.parent );
//                                             if (pp) {
//                                                 if (pp.parent) {
//                                                     let ppp = categories.find( category => category.nanoid === pp.parent );
//                                                     if (ppp) {
//                                                         if (ppp.parent) {
//                                                             console.log(value.tile + ': bad journal parent/not journal');
//                                                         }
//                                                         else {
//                                                             if (ppp.key === 'journal') {
//                                                                 tag = pp.title + p.title + value.title;//d
//                                                             }            
//                                                         }

//                                                     }
//                                                     else {
//                                                         console.error('Bad parent=' + pp.parent);

//                                                     }
//                                                 }
//                                                 else {
//                                                     if (pp.key === 'journal') {
//                                                         tag = p.title + value.title;//m
//                                                     }
        
//                                                 }
//                                             }
//                                             else {
//                                                 console.error('Bad parent=' + p.parent);
//                                             }
        
//                                         }
//                                         else {
//                                             if (p.key === 'journal') {
//                                                 tag = value.title;//y
//                                             }
//                                         }
    
//                                     }
//                                     else {
//                                         console.error('Bad parent=' + value.parent);
//                                     }
//                                     /*await echo.categories.where({nanoid: value.parent}).first().then(
//                                         async (p) => {
//                                             if (p.parent) {
//                                                 await echo.categories.where({nanoid: p.parent}).first().then(
//                                                     async (pp) => {
//                                                         if (pp.parent) {
//                                                             await echo.categories.where({nanoid: pp.parent}).first().then(
//                                                                 async (ppp) => {
//                                                                     if (ppp.parent) {
//                                                                         console.log(value.tile + ': bad journal parent/not journal');
//                                                                     }
//                                                                     else {
//                                                                         if (ppp.key === 'journal') {
//                                                                             tag = pp.title + p.title + value.title;//d
//                                                                         }            
//                                                                     }
//                                                                 }
//                                                             );
//                                                         }
//                                                         else {
//                                                             if (pp.key === 'journal') {
//                                                                 tag = p.title + value.title;//m
//                                                             }
//                                                         }
//                                                     }
//                                                 );
//                                             }
//                                             else {
//                                                 if (p.key === 'journal') {
//                                                     tag = value.title;//y
//                                                 }
//                                             }
//                                         }
//                                     ////);*
//                                     let format;
//                                     if (value.type === 'hybrid') {
//                                         let h = hybrids.find((hybrid) => hybrid.nanoid === value.nanoid);
//                                         if (h) {
//                                             if (h.format) {
//                                                 format = h.format;
//                                             }

//                                         }
//                                         else {
//                                             console.error('Loss hybrid=' + value.nanoid);
//                                         }
//                                         //await echo.hybrids.where({nanoid: value.nanoid}).first().then(
//                                         //    async (h) => {
//                                         //        if (h) {
//                                         //            if (h.format) {
//                                         //                format = h.format;
//                                         //            }
//                                         //        }
//                                         //        else {
//                                         //            console.error('Loss hybrid=' + value.nanoid);
//                                         //        }
//                                         //    }
//                                         //);
//                                     }//nanoid, editor, key, parent, tag, title, type, format, flag
//                                     c = {nanoid: value.nanoid, editor: value.editor, parent: value.parent, title: value.title, type: value.type, flag: value.flag};
//                                     if (tag) {
//                                         c.tag = tag;
//                                     }
//                                     if (format) {
//                                         c.mimetype = 'application/draft-js-raw+json';//format;
//                                     }
//                                 }
//                                 else {
//                                     c = {nanoid: value.nanoid, editor: value.editor, title: value.title, type: value.type, flag: value.flag};
//                                     if (value.key) {
//                                         c.key = value.key;
//                                     }
//                                 }
//                                 if (value.flag) {
//                                     c.flag = value.flag;
//                                 }
//                                 await db.categories.put(c);
//                             }
//                         );
//                     }
//                 });
//                 await echo.hybrids.count(async (count) => {
//                     console.log('hybrids.count=' + count);
//                     if (count) {//hybrids: '&nanoid, data, option, sum', //category, key, title, flag, 
//                         await echo.hybrids.each(
//                             async (value) => {
//                                 let h = {nanoid: value.nanoid};
//                                 if (value.data) {
//                                     h.data = value.data;
//                                 }
//                                 if (value.option) {
//                                     h.option = value.option;
//                                 }
//                                 if (value.sum) {
//                                     h.sum = value.sum;
//                                 }
//                                 if (value.flag) {
//                                     h.flag = value.flag;
//                                 }
//                                 await db.hybrids.put(h);
//                             }
//                         );
//                     }
//                 });
//                 await echo.options.count(async (count) => {
//                     console.log('options.count=' + count);
//                     if (count) {//options: 'key, value, flag', // value: use nanoid or string value.//add flag
//                         await echo.options.each(
//                             async (value) => {
//                                 let o = {key: value.key};
//                                 if (value.value) {
//                                     o.value = value.value;
//                                 }
//                                 if (value.flag) {
//                                     o.flag = value.flag;
//                                 }
//                                 await db.options.put(o);
//                             }
//                         );
//                     }
//                 });
        
//             }
//         });
    
//     }*/

// });

//echo.on('populate', populate);

//application/draft-js
//application/lexical
/*db.on('populate', populate);
db.on('ready', async () => {
    echo.open();
    //db.config.each(value => {
    //    echo.options.put({key: value.key, value: value.value});
    //});
    echo.options.count(async (value) => {
        if (value == 0) {
            await db.editors.each(value => {
                echo.editors.put({nanoid: value.nanoid, key: value.key, flag: value.flag, option: value.option});
                echo.options.put({key: 'editor', value: value.nanoid});
            });
            await db.raws.each(value => {
                if (value.nanoid) {
                    echo.hybrids.put({nanoid: value.nanoid, editor: value.editor, key: value.key, data: value.raw, format: 'draft-js-raw', sum: value.sum});
                    echo.options.put({key: 'hybrid', value: value.nanoid});
                }
                else {
                    let id = nanoid();
                    echo.hybrids.put({nanoid: id, editor: value.editor, key: value.key, data: value.raw, format: 'draft-js-raw', sum: value.sum});
                    echo.options.put({key: 'hybrid', value: id});
                }
            });
        
        }
    });
});*/
