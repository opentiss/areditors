import { fs } from "@zip.js/zip.js";
import { echo } from "./db";

export async function exportEditor(password) {
    //configure({chunkSize: 128, useWebWorkers: false});
    //const zipWriter = new ZipWriter(new BlobWriter("application/zip"));
    let editorNanoid;
    const zipFS = new fs.FS();
    const options = await echo.options.toArray();
    zipFS.addText('options.json', JSON.stringify(options));
    await echo.options.where({key: 'editor'}).first(
        (value) => {
            editorNanoid = value.value;
            //zipWriter.add('options.json', new TextReader(JSON.stringify(value)));
        }
    );
    const editors = await echo.editors.where({nanoid: editorNanoid}).toArray();
    zipFS.addText('editors.json', JSON.stringify(editors));
    const categories = await echo.categories.where({editor: editorNanoid}).toArray();
    zipFS.addText('categories.json', JSON.stringify(categories));
    let directory = zipFS.addDirectory('hybrids');
    //let enc = new TextEncoder();
    // That's bad: the hybrid file zero length.
    // await categories.forEach(async (element) => {
    //     if (element.type === 'hybrid') {
    //         const text = await echo.hybrids.where({nanoid: element.nanoid}).first(
    //             async (value) => JSON.stringify(value)
    //         )
    //         // .then( async (text) => {
    //         //     if (text) {
    //         //         directory.addText(element.nanoid + '.json', text);
    //         //         //directory.addUint8Array(element.nanoid + '.json', enc.encode(text));
    //         //     }
    //         // });
    //         if (text) {
    //             directory.addText(element.nanoid + '.json', text);
    //             //directory.addUint8Array(element.nanoid + '.json', enc.encode(text));
    //         }
    //     }
    // });

    // categories.forEach(async (element) => {
    //     if (element.type === 'hybrid') {
    //         directory.addText(element.nanoid + '.json', JSON.stringify(element));
    //     }
    // });

    // That's OK.
    for (const category in categories) {
        if (categories[category].type === 'hybrid') {
            let text;
            await echo.hybrids.where({nanoid: categories[category].nanoid}).first(
                async (value) => {
                    text = JSON.stringify(value);
                }
            );
            if (text) {
                directory.addText(categories[category].nanoid + '.json', text);
                //directory.addUint8Array(element.nanoid + '.json', enc.encode(text));

            }
        }
    }
    // That's OK.
    //directory.addText('categories_in_directory.json', JSON.stringify(categories));
    if (password) {
        //await zipWriter.close();
        downloadFile(await zipFS.exportBlob({password: password}), editorNanoid);

    }
    // else {
    //     downloadFile(await zipWriter.close(), editorNanoid);

    // }
};

function downloadFile(blob, name) {
    const aTag = Object.assign(document.createElement("a"), {
        download: name + '_' + Date.now() + ".zip",
        href: URL.createObjectURL(blob),
        textContent: "Download editor zip file",
    });
    document.body.appendChild(aTag);
    aTag.click();
    aTag.remove();
}
