const b2au = (s) => {
    return btoa(s).replace(/=+$/,'').replace(/\+/g,'-').replace(/\//g,'_');
};

const a2bu = (s) => {
    return atob(s.replace(/-/g,'+').replace(/_/g,'/'));
};

const ua2b = (u) => {
    const a = [];
    while (a.length < u.length) {
        a.push(u.charCodeAt(a.length));
    }
    return Uint8Array.from(a);
};

const ub2a = (u) => {
    const a = u.buffer ? Array.from(u) : Array.from(new Uint8Array(u));
    return a.map((b) => String.fromCharCode(b)).join('');
};

const REMAINDER = 99999; // 999999/9999999 too long

const encoder = new TextEncoder();
const decoder = new TextDecoder();

export async function generateAESGCM (extractable) { // salt: hash source, key: , passphrase:
    let raw, iv;
    raw = await crypto.subtle.generateKey(
        {
            name: 'AES-GCM',
            length: 256,
        },
        extractable ? true : false,
        ["encrypt", "decrypt"]
    );
    iv = crypto.getRandomValues(new Uint8Array(48));//await crypto.subtle.digest('SHA-256', crypto.getRandomValues(new Uint8Array(16)));
    return {key: raw, iv: iv};
};

export async function generatePBKAESGCM (passphrase, u, remainder) { // salt: hash source, key: , passphrase:
    let raw, iv;
    let salt; // extractable
    salt = u ? ua2b(a2bu(u)) : crypto.getRandomValues(new Uint8Array(48));
    const iterationArray = Array.from(new Uint32Array(salt));//Array.from(new Uint32Array(hashBuffer));
    let iterations = iterationArray[0];
    iterations = iterationArray[iterations % 8];
    let pbk = await crypto.subtle.importKey(
        "raw",
        encoder.encode(passphrase),
        {
            name: "PBKDF2"
        },
        false,
        ["deriveBits", "deriveKey"]
    );
    iterations = Math.abs(iterations % (Number.isInteger(remainder) ? (REMAINDER > remainder ? REMAINDER : remainder) : REMAINDER)) + (Number.isInteger(remainder) ? (REMAINDER > remainder ? REMAINDER : remainder) : REMAINDER);
    iterations = iterations * 8;
    raw = await crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: salt,
            "iterations": iterations,
            "hash": "SHA-512"
        },
        pbk,
        {
            "name": "AES-GCM",
            "length": 256
        },
        false,
        [ "encrypt", "decrypt" ]
    );
    iv = await crypto.subtle.deriveBits(
        {
            "name": "PBKDF2",
            salt: salt,
            "iterations": Math.floor(iterations / 9) * 8,
            "hash": "SHA-256"
        },
        pbk,
        512,
    );

    if (u) {
        return {key: raw, iv: new Uint8Array(iv)};
    }
    else {
        return {key: raw, iv: new Uint8Array(iv), salt: salt};
    }
};

export async function encryptAESGCM(a, encodedPlaintext, encode) {
    // {crypto:{pku:BASE64URL,eku:BASE64URL,ctu:BASE64URL}}
    // pku: BASE64URL(PBKDF2 salt).BASE64URL(PBKDF2 encrypted master AESGCM key and iv: plaintext=BASE64URL(key).BASE64URL(iv)).
    // gku: BASE64URL(master AESGCM key encrypted generic AESGCM key and iv: plaintext=BASE64URL(key).BASE64URL(iv)).
    // eku: BASE64URL(master AESGCM key encrypted random AESGCM key and iv: plaintext=BASE64URL(key).BASE64URL(iv)).
    // ctu: BASE64URL(random AESGCM key encrypted ciphertext).
    // Usage: Editor option stores pku and gku(for no option text context).
    //        Hybrid option stores eku and data saved in ctu.
    const c = await crypto.subtle.encrypt(
        {
            name: "AES-GCM",
            iv: a.iv
        },
        a.key,
        encodedPlaintext
    );
    if (encode) {
        return b2au(ub2a(c));
    }
    else {
        return c;
    }
};

export async function decryptAESGCM(a, b64uCiphertext, decode) {
    let e = false;
    const p = await crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: a.iv
        },
        a.key,
        ua2b(a2bu(b64uCiphertext))
    ).then(
        (value) => {
            return value;
        }
    ).catch(
        (err) => {
            //console.error(err);
            e = !!err;
        }
    );
    if (e) {
        return !e;
    }
    else {
        if (decode) {
            return decoder.decode(p);
        }
        else {
            return p;
        }
    }
};

export async function exportAESGCM(a) {
    const exported = await crypto.subtle.exportKey('raw', a.key);
    return exported;
};

export async function importAESGCM(a) { // extractable: import for decrypt with false, key only used once for encrypt.
    const imported = await crypto.subtle.importKey(
        'raw',
        a,
        'AES-GCM',
        false,
        ['encrypt', 'decrypt']
    );
    return imported;
};

export async function derivePBKMK (pbk, ciphertext) { // Masterkey export in editor option.
    let mk, raw;
    if (!ciphertext) {
        mk = generateAESGCM(true);
        raw = exportAESGCM(mk.key);
    }
    else {
        ;
    }
    mk = importAESGCM(raw, false);
    return mk;
};

export async function deriveKey(passphrase) {
    const pbks = await generatePBKAESGCM (passphrase);
    const ek = await generateAESGCM(true);
    const kr = await exportAESGCM(ek);
    const k = await importAESGCM(kr);
    ek.key = k;
    //console.log(b2au(ub2a(kr)) + '.' + b2au(ub2a(ek.iv)));
    //ua2b(a2bu())//
    const er = await encryptAESGCM({key: pbks.key, iv: pbks.iv }, encoder.encode(b2au(ub2a(kr)) + '.' + b2au(ub2a(ek.iv))));
    //console.log(b2au(ub2a(kr)) + '.' + b2au(ub2a(ek.iv)));
    return {crypto: b2au(ub2a(pbks.salt)) + '.' + b2au(ub2a(er)), k: {key: k, iv: ek.iv}};
}

export async function changePBK(passphrase, u, n) {
    let us = u.split('.');
    if (us.length > 1) {
        const pbks = await generatePBKAESGCM (passphrase, us[0]);
        const v = await decryptAESGCM({key: pbks.key, iv: pbks.iv }, us[1]);
        //const v = await decryptAESGCM({key: pbks.key, iv: pbks.iv }, us[1], true);
        if (v) {
            const pbkn = await generatePBKAESGCM (n);
            const er = await encryptAESGCM({key: pbkn.key, iv: pbkn.iv }, v);
            //const er = await encryptAESGCM({key: pbkn.key, iv: pbkn.iv }, encoder.encode(v));
            return {crypto: b2au(ub2a(pbkn.salt)) + '.' + b2au(ub2a(er))};
        }
        
    }
    return {fail: true};
}

export async function getPBKAESGCM(passphrase, u) {
    let us = u.split('.');
    if (us.length > 1) {
        const pbks = await generatePBKAESGCM (passphrase, us[0]);
        const v = await decryptAESGCM({key: pbks.key, iv: pbks.iv }, us[1], true);
        if (v) {
            let uk = v.split('.');
            const k = await importAESGCM(ua2b(a2bu(uk[0])));
            return {key: k, iv: ua2b(a2bu(uk[1]))};
        }
        
    }
    return {fail: true};
}

export async function encryptAESGCMChain(p, u) { // for long item.
    const ek = await generateAESGCM(true);
    const kr = await exportAESGCM(ek);
    const er = await encryptAESGCM({ key: p.key, iv: p.iv }, encoder.encode(b2au(ub2a(kr)) + '.' + b2au(ub2a(ek.iv))), true);
    const ct = await encryptAESGCM(ek, encoder.encode(u), true);
    return {eku: er, ctu: ct};
}

export async function decryptAESGCMChain(p, k, t) { // for long item.
    const v = await decryptAESGCM(p, k, true);
    if (v) {
        let uk = v.split('.');
        const k = await importAESGCM(ua2b(a2bu(uk[0])));
        let u = await decryptAESGCM({key: k, iv: ua2b(a2bu(uk[1]))}, t, true);
        return u;
    }
    return {fail: true};
}

export async function encryptAESGCMChains(passphrase, u) { // for short item.
    let us = u.split('.');
    if (us.length > 1) {
        const pbks = await generatePBKAESGCM (passphrase, us[0]);
        const v = await decryptAESGCM({key: pbks.key, iv: pbks.iv }, us[1], true);
        if (v) {
            let uk = v.split('.');
            const k = await importAESGCM(ua2b(a2bu(uk[0])));
            return {key: k, iv: ua2b(a2bu(uk[1]))};
        }
        
    }
    return {fail: true};
}
