// Arekey try to solve no serializable data or crypto key.
// All secure data or crypto key keep in single place.
// Arekey data has lifetime for secure used.

import { changePBK, decryptAESGCMChain, deriveKey, encryptAESGCMChain, getPBKAESGCM } from "../models/crypto";

export default class Arekey {
  static #isInternalConstructing = false;
  static #keys = [];
  static #pendings = {}; // pending actions.

  #crypto;
  //#data;// Save one time key and data. {key: {}, items: []}
  #nanoid;

  #lifetime = 360000; // The longest lifetime(milliseconds).
  #masterkey; // Saved key with lifetime.
  #timestamp = 0;

  constructor(editor) {
    if (!Arekey.#isInternalConstructing) {
      throw new TypeError("Aresync is not constructable");
    } else {
      if (editor.crypto) {
        this.#crypto = editor.crypto;
      }
      if (editor.nanoid) {
        this.#nanoid = editor.nanoid;
      }
      if (editor.lifetime && editor.lifetime < this.#lifetime) {
        this.#lifetime = editor.lifetime;
      }
    }
  }

  static creator(editor) {//{nanoid,crypto}
    Arekey.#isInternalConstructing = true;
    let arekey = new Arekey(editor);
    if (Arekey.#keys.length && Arekey.has(editor)) {
      arekey = Arekey.#keys.find(
        (k) => k.isNanoid(editor.nanoid)
      );
      arekey.clear(editor);
    }
    else {
      arekey = new Arekey(editor);
      Arekey.#keys.push(arekey);
    }
    Arekey.#isInternalConstructing = false;
    //return arekey; // Only use static to use the instance.
  }

  static async activate(editor) {
    let key = Arekey.#keys.find(
      (k) => k.isNanoid(editor.nanoid)
    );
    if (key) {
      if (editor.passphrase) {
        return await key.live(editor.passphrase);
      } else {
        return key.isLive();
      }
    } else {
      return false;
    }
  }

  static async addPendings(editor) {
    if (editor.nanoid && editor.pendings) {
      let pendings = editor.pendings;
      if (Arekey.#pendings[editor.nanoid]) {
        if (Array.isArray(pendings)) {
          Arekey.#pendings[editor.nanoid].push(...pendings);
        }
        else {
          Arekey.#pendings[editor.nanoid].push(pendings);
        }
      }
      else {
        if (Array.isArray(pendings)) {
          Arekey.#pendings[editor.nanoid] = [...pendings];
        }
        else {
          Arekey.#pendings[editor.nanoid] = [pendings];
        }
      }
    }
  }

  static async decrypt(editor) {
    let key = Arekey.#keys.find(
      (k) => k.isNanoid(editor.nanoid)
    );
    if (key) {
      return await key.act(editor);
    } else {
      return false;
    }
  }

  static async encrypt(editor) {
    let key = Arekey.#keys.find(
      (k) => k.isNanoid(editor.nanoid)
    );
    if (key) {
      return await key.act(editor, true);
    } else {
      return false;
    }
  }

  static async generate(editor) {
    if (editor.nanoid && editor.passphrase) {
      let key = Arekey.#keys.find(
        (k) => k.isNanoid(editor.nanoid)
      );
      if (!key) {
        Arekey.creator(editor);
      }
      key = Arekey.#keys.find(
        (k) => k.isNanoid(editor.nanoid)
      );
      return await key.derive(editor.passphrase);
    } else {
      return false;
    }
  }

  static has(editor) {
    return (Arekey.#keys.find(
      (k) => k.isNanoid(editor.nanoid)
    ) !== undefined);
  }

  static hasLive(editor) {
    let key = Arekey.#keys.find(
      (k) => k.isNanoid(editor.nanoid)
    );
    if (key) {
      if (key.isLive(1000)) {
        return true; // To guarantee enough buffer(1000 milliseconds) for action.
      }
      else {
        if (editor.pendings) {
          Arekey.addPendings(editor);
        }
        return false;
      }
    } else {
      return false;
    }
  }

  static async update(editor) {
    if (editor.nanoid && editor.passphrase && editor.newpassphrase) {
      let key = Arekey.#keys.find(
        (k) => k.isNanoid(editor.nanoid)
      );
      if (!key) {
        Arekey.creator(editor);
      }
      key = Arekey.#keys.find(
        (k) => k.isNanoid(editor.nanoid)
      );
      if (key) {
        return await key.updatePBK(editor);
      } else {
        return {fail: true};
      }
    } else {
      return {fail: true};
    }
  }

  async act(editor, encrypt) {
    if (this.isLive()) {
      this.#timestamp = Date.now();
      if (encrypt) {
        return await encryptAESGCMChain(this.#masterkey, editor.plain);
      }
      else {
        return await decryptAESGCMChain(this.#masterkey, editor.eku, editor.ctu);
      }
    } else {
      return false;
    }
  }

  clear(editor) {
    this.#crypto = undefined;
    //this.#data = undefined;
    this.#nanoid = undefined;
    if (editor.crypto) {
      this.#crypto = editor.crypto;
    }
    if (editor.nanoid) {
      this.#nanoid = editor.nanoid;
    }
    if (editor.lifetime && editor.lifetime < this.#lifetime) {
      this.#lifetime = editor.lifetime;
    }
  }

  async derive(passphrase) {
    this.#timestamp = Date.now();
    let key = await deriveKey(passphrase);
    this.#masterkey = key.k;
    return {crypto: key.crypto};
  }

  isLive(actionBuffer = 0) {
    if ((Date.now() - this.#lifetime - actionBuffer) < this.#timestamp) {
      return true;
    } else {
      this.#masterkey = undefined;
      //this.#data = undefined;
    }
  }

  isNanoid(id) {
    return this.#nanoid === id;
  }

  async live(passphrase) {
    if (passphrase) {
      let k = await getPBKAESGCM(passphrase, this.#crypto);
      if (!k.fail) {
        this.#timestamp = Date.now();
        this.#masterkey = k;
        // if (this.#crypto.ctu) {
        //   this.#data = await decryptAESGCMChain(k, this.#crypto.eku, this.#crypto.ctu);
        // }
        while (Arekey.#pendings[this.#nanoid]?.length) {
          let pending = Arekey.#pendings[this.#nanoid].shift();
          if (typeof pending === 'function') {
            pending.call();
          }
        }
        return true;
      } else {
        this.#timestamp = 0;
        return false;
      }
    } else {
      return false;
    }
  }

  async updatePBK(editor) {
    if (editor.nanoid && editor.passphrase && editor.newpassphrase) {
      let key = await changePBK(editor.passphrase, this.#crypto, editor.newpassphrase);
      if (!key.fail) {
        this.#crypto = key.crypto;
        await this.live(editor.newpassphrase);
        return key;
      } else {
        return {fail: true};
      }
    } else {
      return {fail: true};
    }
  }
};
