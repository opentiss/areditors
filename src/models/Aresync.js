
export default class Aresync {
  static #isInternalConstructing = false;
  static #syncs = [];
  #channels = [];// sync
  #connections = [];
  #credential;
  #data = [];
  #events = {};
  #groups = [];
  #id;
  #name;
  #signaling;
  #stun;
  #username;
  #ws;

  constructor(configuration) {
    if (!Aresync.#isInternalConstructing) {
      throw new TypeError("Aresync is not constructable");
    } else {
      if (configuration.name) {
        this.#name = configuration.name;
      }
      if (configuration.signaling) {
        this.#signaling = configuration.signaling;
      }
      if (configuration.stun) {
        this.#stun = configuration.stun;
      }
      if (configuration.username) {
        this.#username = configuration.username;
      }
      if (configuration.credential) {
        this.#credential = configuration.credential;
      }
      if (configuration.events) {
        this.#events = Object.assign(this.#events, configuration.events);
      }
    }
  }

  static creator(configuration) {
    Aresync.#isInternalConstructing = true;
    let aresync = new Aresync(configuration);
    Aresync.#syncs.push(aresync);
    Aresync.#isInternalConstructing = false;
    return aresync;
  }

  static getAresync(id) {
    let aresync;
    for (let i = 0; i < Aresync.#syncs.length; i++) {
      if (Aresync.#syncs[i].is(id)) {
        aresync = Aresync.#syncs[i];
      }
    }
    return aresync;
  }

  addEvent(event, action) {
    this.#events[event] = action;
  }

  is (id) {
    return (id === this.#id);
  }

  async send(configuration) {
    if (configuration.channel === 'messaging') {
      let dataChannel = this.#channels.find(
        (i) => i.target === configuration.target && i.type === configuration.channel
      );
      if (dataChannel) {
        dataChannel = dataChannel.channel;
        if (dataChannel.readyState === 'open') {
          dataChannel.send(configuration.data);
        }
      }
    }
    else if (!configuration.channel) {// data={data/file}, type={data/file}, meta={lastModified, name, size, type}
      let metaChannel = this.#channels.find(
        (i) => i.target === configuration.target && i.type === 'meta'
      );
      if (metaChannel) {
        metaChannel = metaChannel.channel;
        const data = new Uint32Array(10);
        crypto.getRandomValues(data);
        let hash = await crypto.subtle.digest('SHA-256', data);
        let id = Array.from(new Uint8Array(hash)).map((b) => b.toString(16).padStart(2, '0')).join('').substring(1, 9).toUpperCase();
        this.#data.push({id: id, status: 'connecting'});
        metaChannel.send(JSON.stringify({id: id, type: configuration.type, meta: {...configuration.meta}}));
      }
    }
  }

  signal(msg) {
    if (this.#ws) {
      this.#ws.send(JSON.stringify(msg));
    }
  }

  setUsername(group, passphrase) {
    const msg = {
      date: Date.now(),
      id: this.#id,
      type: "username"
    };

    if (group) {
      msg.group = group;
    }
    if (passphrase) {
      msg.credential = passphrase;
    }

    if (this.#name) {
      msg.name = this.#name;
    }

    this.signal(msg);
  }

  updateGroups(msg) {
    let group = this.#groups.find(i => i.group === msg.group);
    if (group) {
      // Merge and tag peer remote user as signaloss.
      group.users.forEach(
        (u) => {
          let mu = msg.users.find(
            (m) => m.id === u.id
          );
          if (mu) {
            u = {...u, ...mu};
          }
          else {
            u.signaloss = true;
          }
        }
      );
      msg.users.forEach(
        (u) => {
          let gu = group.users.find(
            (g) => g.id === u.id
          );
          if (!gu) {
            group.users.push(u);
          }
        }
      );
    }
    else {
      let id = this.#id;
      msg.users.forEach(
        (u) => {
          if (u.id === id) {
            u.current = true;
          }
        }
      );
      msg.users.sort(
        (a, b) => a.current ? -1 : b.current ? 1 : 0
      );
      group = {group: msg.group, users: msg.users};
      this.#groups.push(group);
    }
  }

  async connect(configuration) {
    const events = this.#events;
    let group;
    if (configuration?.group?.length) {
      group = configuration.group;
    }
    else {
      const data = new Uint32Array(10);
      crypto.getRandomValues(data);
      let hash = await crypto.subtle.digest('SHA-256', data);
      group = Array.from(new Uint8Array(hash)).map((b) => b.toString(16).padStart(2, '0')).join('').substring(1, 9).toUpperCase();
    }

    this.#ws = new WebSocket(this.#signaling, "json");

    this.#ws.onclose = (evt) => {
      if (events?.onsignalingclose) {
        events.onsignalingclose(evt);
      }
    };

    this.#ws.onerror = (evt) => {
      if (events?.onsignalingerror) {
        events.onsignalingerror(evt);
      }
    };

    this.#ws.onopen = (evt) => {
      if (events?.onsignalingopen) {
        events.onsignalingopen(evt);
      }
    };

    this.#ws.onmessage = (evt) => {
      const msg = JSON.parse(evt.data);
      switch(msg.type) {
        case "id":
          this.#id = msg.id;
          this.setUsername(group, configuration.passphrase);
          if (this.#events.onid) {
            this.#events.onid(this.#id);
          }
          else if (configuration?.onid) {
            configuration?.onid(this.#id);
          }
          break;

        case "userlist":
          this.updateGroups(msg);
          if (this.#events.onuserlist) {
            this.#events.onuserlist(this.#groups);
          }
          else if (configuration?.onuserlist) {
            configuration?.onuserlist(this.#groups);
          }
          break;

        case "offer":
          this.offer(msg);
          if (this.#events.onoffer) {
            this.#events.onoffer(msg);
          }
          break;

        case "answer":
          this.answer(msg);
          if (this.#events.onanswer) {
            this.#events.onanswer(msg);
          }
          break;

        case "new-ice-candidate":
          this.newICECandidate(msg);
          break;
        default:
          break;
      }

      if (events?.onsignalingmessage) {
        events.onsignalingmessage(evt);
      }
    };
  }

  disconnect() {
    if (this.#ws) {
      this.#ws.close();
      this.#ws = undefined;
    }
  }

  async invite(configuration) {
    const events = this.#events;
    let config = {id: this.#id, name: configuration.name, target: configuration.target};
    if (configuration.certificate) {
      config.certificates = [await RTCPeerConnection.generateCertificate({
        name: "ECDSA",
        namedCurve: "P-256"
      })];
    }
    if (configuration.events) {
      config.events = configuration.events;
    }
    if (configuration.credential) {
      config.credential = configuration.credential;
    }
    if (configuration.username) {
      config.username = configuration.username;
    }

    let peerConnection = await this.createPeerConnection(config);
    if (configuration.channels.indexOf('messaging') !== -1) {
      let dataChannel = peerConnection.createDataChannel("messaging");
      dataChannel.onmessage = (evt) => {
        if (evt.type === 'message') {
          if (events?.onmessage) {
            events.onmessage({channel: evt.target.label, data: evt.data});
          }
          else if (configuration?.events?.onmessage) {
            configuration.events.onmessage({channel: evt.target.label, data: evt.data});
          }
        }
      }
      this.#channels.push({target: configuration.target, type: 'messaging', channel: dataChannel});
    }
    if (configuration.channels.indexOf('data') !== -1) {
      // First use meta define data attributes, then deliver data.
      let metaChannel = peerConnection.createDataChannel("meta");
      metaChannel.onmessage = (evt) => {
        if (evt.type === 'message') {
          //let msg = JSON.parse(evt.data);
        }
      }
      this.#channels.push({target: configuration.target, type: 'meta', channel: metaChannel});
      let dataChannel = peerConnection.createDataChannel("data");
      dataChannel.onmessage = (evt) => {
        if (evt.type === 'message') {
          if (events?.onmessage) {
            events.onmessage({channel: evt.target.label, data: evt.data});
          }
        }
      }
      this.#channels.push({target: configuration.target, type: 'data', channel: dataChannel});
    }

  }

  async createPeerConnection(configuration) {
    const events = this.#events;
    const data = this.#data;
    const channels = this.#channels;
    const connections = this.#connections;
    let iceServers = [{
      urls: this.#stun,
      username: configuration.username ? configuration.username : this.#username, //"webrtc",
      credential: configuration.credential ? configuration.credential : this.#credential, //"turnserver"
    }];
    if (configuration.iceServers) {
      iceServers = configuration.iceServers;
    }
    const config = {iceServers: iceServers};
    if (configuration.certificates) {
      config.certificates = configuration.certificates;
    }

    let peerConnection = new RTCPeerConnection(config);

    peerConnection.onconnectionstatechange = (evt) => {
      if (events?.onconnectionstatechange) {
        events.onconnectionstatechange(evt);
      }
      switch(evt.target.connectionState) {
        case 'closed':
        case 'disconnected':
        case 'failed':
          this.closePeerConnection(configuration.target);
          peerConnection.onconnectionstatechange = undefined;
          peerConnection.onicecandidate = undefined;
          peerConnection.oniceconnectionstatechange = undefined;
          peerConnection.onicegatheringstatechange = undefined;
          peerConnection.onsignalingstatechange = undefined;
          peerConnection.onnegotiationneeded = undefined;
          peerConnection.ontrack = undefined;
          peerConnection.ondatachannel = undefined;
          peerConnection = undefined;
          break;
        default:
          break;
      }
    };
    peerConnection.onicecandidate = (evt) => {
      if (evt.candidate) {
        this.signal({
          type: "new-ice-candidate",
          id: this.#id,
          target: configuration.target,
          candidate: evt.candidate
        });
      }
    };
    peerConnection.oniceconnectionstatechange = (evt) => {
      // switch(peerConnection.iceConnectionState) {
      //   case "closed":
      //   case "failed":
      //   case "disconnected":
      //     //closeVideoCall(configuration.target);
      //     break;
      // }
    };
    peerConnection.onicegatheringstatechange = (evt) => {
      ;
    };
    peerConnection.onsignalingstatechange = (evt) => {
      if (events?.onsignalingstatechange) {
        events.onsignalingstatechange(evt);
      }
      // switch(peerConnection.signalingState) {
      //   case 'closed':
      //     this.removeSignaloss(configuration.target);
      //     if (this.#events.onuserlist) {
      //       this.#events.onuserlist(this.#groups);
      //     }
      //     this.#connections.splice(
      //       this.#connections.findIndex(
      //         (c) => c.remote.id === configuration.target
      //       ),
      //       1
      //     );
      //     peerConnection = undefined;
      //     break;
      //   default:
      //     break;
      // }
    };
    peerConnection.onnegotiationneeded = async (evt) => {
      try {
        const offer = await peerConnection.createOffer();
        if (peerConnection.signalingState !== "stable") {
          return;
        }
        await peerConnection.setLocalDescription(offer);
        this.signal({
          id: this.#id,
          name: this.#name,
          target: configuration.target,
          type: "offer",
          sdp: peerConnection.localDescription
        });
          
      } catch (error) {
        console.error(error);
      }
    };
    peerConnection.ontrack = (evt) => {
      ;
    };
    peerConnection.ondatachannel = (evt) => {
      let receiveChannel = evt.channel;
      console.log('binaryType=' + receiveChannel.binaryType);
      if (events?.ondatachannel) {
        events.ondatachannel({channel: receiveChannel.label});
      }
      receiveChannel.onmessage = (message) => {
        if (message.type === 'message') {
          if (message.target.label === 'meta') {
            let msg = JSON.parse(message.data);
            if (msg.status === 'connecting') {
              data.push({...msg, status: 'open'});
              receiveChannel.send(JSON.stringify({...msg, status: 'open'}));
            }
            else if (msg.status === 'open') {
              //data.push({...msg, status: 'open'});
              let dataChannel = channels.find(
                (i) => i.channel === 'data'
              );
              if (dataChannel) {
                const c = connections.find(
                  (i) => i.remote.id === dataChannel.target
                );
                if (c) {
                  // dataChannel = dataChannel.channel;
                  // const d = data.find(
                  //   (i) => i.id === msg.id
                  // );
                  // const maxMessageSize = c.sctp.maxMessageSize;
                  // dataChannel.send();
                }
              }
            }

          }
          if (events?.onmessage) {
            events.onmessage({channel: message.target.label, data: message.data});
          }
          else if (configuration?.events?.onmessage) {
            configuration.events.onmessage({channel: evt.target.label, data: message.data});
          }
        }
      };
      this.#channels.push({target: configuration.target, type: receiveChannel.label, channel: receiveChannel})

    };

    this.#connections.push({group: '', local: {account: false, id: 0, name: ''}, remote: {account: false, id: configuration.target, name: ''}, connection: peerConnection});
    return peerConnection;
  }

  removeSignaloss(target) {
    for (let i = 0; i < this.#groups.length; i++) {
      let group = this.#groups[i];
      let signaloss;
      while ((signaloss = group.users.findIndex(
        (u) => (u.id === target) && u.signaloss
      )) !== -1) {
        group.users.splice(signaloss, 1);
      }
    }
  }

  closePeerConnection(target) {
    this.removeSignaloss(target);
    if (this.#events.onuserlist) {
      this.#events.onuserlist(this.#groups);
    }
    let ci = this.#connections.findIndex(
      (c) => c.remote.id === target
    );
    if (ci !== -1) {
      let peerConnection = this.#connections[ci];
      if (peerConnection) {
        this.removeSignaloss(target);
        if (this.#events.onuserlist) {
          this.#events.onuserlist(this.#groups);
        }
        if (peerConnection) {
          if (peerConnection.connection.connectionState === 'connected') {
            peerConnection.connection.close();
          }
          peerConnection.connection = undefined;
        }

        this.#connections.splice(
          ci,
          1
        );

        peerConnection = undefined;
      }
    }
    while ((ci = this.#channels.findIndex(
      (c) => c.target === target
    )) !== -1) {
      this.#channels.splice(
        ci,
        1
      );
    }
  }

  async offer(msg) {
    let targetUsername = msg.name;
    let target = msg.id;
    let peerConnection = this.#connections.find(
      (c) => c.remote.id === target
    );
    if (peerConnection) {
      peerConnection = peerConnection.connection;
    }
    else {
      peerConnection = await this.createPeerConnection({id: this.#id, name: targetUsername, target: target});
    }
    let desc = new RTCSessionDescription(msg.sdp);
    if (peerConnection.signalingState !== "stable") {
      await Promise.all([
        peerConnection.setLocalDescription({type: "rollback"}),
        peerConnection.setRemoteDescription(desc)
      ]);
      return;
    } else {
      peerConnection.setRemoteDescription(desc);
    }
    await peerConnection.setLocalDescription(await peerConnection.createAnswer());
    this.signal({
      id: this.#id,
      name: this.#name,
      target: target,
      type: "answer",
      sdp: peerConnection.localDescription
    });
  }

  async answer(msg) {
    let desc = new RTCSessionDescription(msg.sdp);
    let peerConnection = this.#connections.find(
      (c) => c.remote.id === msg.id
    );
    if (peerConnection) {
      peerConnection = peerConnection.connection;
    }
    if (peerConnection.signalingState !== "stable") {
      await peerConnection.setRemoteDescription(desc);
    }
  }

  async newICECandidate(msg) {
    let candidate = new RTCIceCandidate(msg.candidate);
    let peerConnection = this.#connections.find(
      (c) => c.remote.id === msg.id
    );
    if (peerConnection) {
      peerConnection = peerConnection.connection;
      try {
        await peerConnection.addIceCandidate(candidate);
      } catch (error) {
        console.error(error);
      }
    }
  }
};
