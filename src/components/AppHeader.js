import { useEffect, useState } from "react";
import { AppBar, Avatar, Divider, IconButton, ListItemIcon, Menu, MenuItem, Toolbar, Tooltip, Typography } from "@mui/material";
import ImportExport from '@mui/icons-material/ImportExport'
import { Lock, LockOpen, PersonAdd, Save, Security, Settings, SwitchAccount } from "@mui/icons-material";
import { useDispatch, useSelector } from "react-redux";
import { autoSaveToggled, cryptoHybrid, fetchHybrid } from '../features/content/contentStateSlice';
import { openDialogImportExport, openDialogProfile, openDialogSecurity } from "../features/action/actionStateSlice";
import logo from '../logo.svg';
import { fetchCategories } from "../features/categories/categoriesSlice";
import ReactAdsense from "@pkasila/react-adsense";

export default function AppHeader(props) {
    const dispatch = useDispatch();
    const editorNanoid = useSelector((state) => {
        return state?.options?.editor?.value
    });
    const editorTitle = useSelector((state) => {
      return state?.options?.editor?.title
  });
  const hybridNanoid = useSelector((state) => {
      return state?.options?.hybrid?.value
    });
  
    const contentChanged = useSelector((state) => state?.contentState?.contentChanged);
    const dialogImportExport = useSelector((state) => state?.action?.actionImportExport?.dialog);
    const dialogProfile = useSelector((state) => state?.action?.actionProfile?.dialog);
    const dialogSecurity = useSelector((state) => state?.action?.actionSecurity?.dialog);
    const autoSave = useSelector((state) => state?.contentState?.autoSave);
    const hybrid = useSelector((state) => state?.contentState?.hybrid);
    const crypto = useSelector((state) => state?.contentState?.hybrid?.option?.crypto);

    useEffect(
      () => {
        if (editorNanoid) {
          dispatch(fetchCategories(editorNanoid));
        }
      },
      [editorNanoid, dispatch]
    );

    useEffect (
      () => {
          if (hybridNanoid) {
            dispatch(fetchHybrid(hybridNanoid));
          }
      },
      [hybridNanoid, dispatch]
    );

    const save = () => {
        if (!autoSave) {
            dispatch(autoSaveToggled());
        }
    };

    const showPassword = () => {
      const {option, ...others} = hybrid;
      if (option) {
        dispatch(cryptoHybrid({...others, option: {...option, crypto: {}}}));
      }
      else {
        dispatch(cryptoHybrid({...others, option: {crypto: {}}}));
      }
    };

    const openDialog = () => {
        if (!dialogImportExport) {
            dispatch(openDialogImportExport(true));
        }
    };

    const openProfile = () => {
        if (!dialogProfile) {
            dispatch(openDialogProfile(true));
        }
    };

    const openSecurity = () => {
      if (!dialogSecurity) {
        dispatch(openDialogSecurity(true));
      }
    };

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };

    if (editorNanoid) {
        /*const today = new Date();
                        {editorNanoid}
        const year = today.getFullYear();
        const month = (today.getMonth() + 1).toString().padStart(2, 0);
        const date = today.getDate().toString().padStart(2, 0);
        console.log(year + ':' + month + ':' + date);*/
        // onClick={props.saveRaw}
    
        return (
                <AppBar position='static'>
                    
                <Toolbar sx={{paddingLeft: 0, paddingRight: 0}}>
                  <img src={logo} className="App-logo" alt="logo" />
                    <IconButton disabled={!!crypto} onClick={showPassword}>
                        <Lock></Lock>
                    </IconButton>
                    <Tooltip title='No implement.'>
                      <span>
                      <IconButton disabled={!crypto}>
                        <LockOpen></LockOpen>
                      </IconButton>
                      </span>
                    </Tooltip>
                    <IconButton onClick={save} disabled={!contentChanged}>
                        <Save></Save>
                    </IconButton>
                    <IconButton disabled={contentChanged} onClick={openDialog}>
                        <ImportExport></ImportExport>
                    </IconButton>
                    { window.location.hostname === 'areditors.opentiss.uk' && (<ReactAdsense client={'ca-pub-6337177885061052'}
                    format={''}
                    slot={'1562485031'}
                    style={{width: 468, height: 60}} />)
                    }
                    <Typography
                    variant="h6"
                    noWrap
                    component="div"
                    sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                    >
                      {editorTitle}
                    </Typography>
                    <Tooltip title="Editor settings">
                        <IconButton
                            onClick={handleClick}
                            aria-controls={open ? 'account-menu' : undefined}
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                        >
                            <Avatar></Avatar>
                        </IconButton>

                    </Tooltip>
                </Toolbar>
                <Menu
                      anchorEl={anchorEl}
                      id="account-menu"
                      open={open}
                      onClose={handleClose}
                      onClick={handleClose}
                      PaperProps={{
                        elevation: 0,
                        sx: {
                          overflow: 'visible',
                          filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                          mt: 1.5,
                          '& .MuiAvatar-root': {
                            width: 32,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                          },
                          '&:before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 0,
                            right: 14,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                          },
                        },
                      }}
                      transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                      anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                    >
                      <MenuItem onClick={openProfile}>
                        <Avatar /> Profile
                      </MenuItem>
                      <MenuItem disabled>
                        <Avatar /> My editor
                      </MenuItem>
                      <MenuItem onClick={openSecurity}>
                        <ListItemIcon>
                          <Security fontSize="small" />
                        </ListItemIcon>
                        Security
                      </MenuItem>
                      <Divider />
                      <MenuItem disabled>
                        <ListItemIcon>
                          <PersonAdd fontSize="small" />
                        </ListItemIcon>
                        Add another editor
                      </MenuItem>
                      <MenuItem disabled>
                        <ListItemIcon>
                          <Settings fontSize="small" />
                        </ListItemIcon>
                        Settings
                      </MenuItem>
                      <MenuItem disabled>
                        <ListItemIcon>
                          <SwitchAccount fontSize="small" />
                        </ListItemIcon>
                        Switch editor
                      </MenuItem>
                    </Menu>

                </AppBar>

        )
    }
}
