import { styled } from '@mui/material/styles';
import { Edit, Group, Link, LinkOff, ManageAccounts, Person, PersonOff, Public, PublicOff, VerifiedUser, Visibility, VisibilityOff } from "@mui/icons-material";
import { Box, Checkbox, FormControl, IconButton, InputAdornment, InputLabel, ListSubheader, MenuItem, OutlinedInput, Paper, Select, TextField, Tooltip, Typography } from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import Aresync from '../models/Aresync';
import { openDialogPassword, openDialogSecurity, setConnect, setSignaling, setSignalingid, setTargetid } from '../features/action/actionStateSlice';
import { useEffect, useState } from 'react';
import Arekey from '../models/Arekey';
import { changeEditorOption } from '../features/options/optionsSlice';

const Peers = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
  display: 'flex',
  flexDirection: 'row',
}));

export default function SyncSettings(props) {
  const dispatch = useDispatch();

  const editor = useSelector((state) => state?.options?.editor);
  const psi = useSelector((state) => state?.options?.psi);
  const sync = useSelector(state => state?.activityBar?.actions?.sync);
  const signaling = useSelector(state => state?.action?.actionSync?.signaling);
  const signalingid = useSelector(state => state?.action?.actionSync?.signalingid);
  const targetid = useSelector(state => state?.action?.actionSync?.targetid);
  const connect = useSelector(state => state?.action?.actionSync?.connect);
  const [account, setAccount] = useState(false);
  const [groups, setGroups] = useState([]);
  const [loadOption, setLoadOption] = useState(true);

  const [values, setValues] = useState({
    credential: '',
    ice: '',
    group: '',
    passphrase: '',
    psi: false,
    signaling: '',
    signalingUsername: '',
    showCredential: false,
    showPassphrase: false,
    username: '',
  });

  useEffect(
    () => {
      if (sync) {
        if (!Arekey.hasLive({nanoid: editor.value})) {
          const loadValues = async () => {
            if (loadOption) {
              if (editor?.option?.crypto?.ctu) {
                let plain = await Arekey.decrypt({nanoid: editor.value, ctu: editor.option.crypto.ctu, eku: editor.option.crypto.eku});
                let option = JSON.parse(plain);
                if (option.sync.service) {
                  if (psi) {
                    setValues({ ...values, ...option.sync.service});
                  } else {
                    setValues({ ...values, ...option.sync.service, psi: false });
                  }
                }
              } else {
                if (psi) {
                  setValues({ ...values, psi: true});
                }
              }
              setLoadOption(false);
            }
          }
        
          Arekey.addPendings({nanoid: editor.value, pendings: loadValues});
          if (editor?.option?.crypto) {
            dispatch(openDialogPassword(true));
          } else {
            dispatch(openDialogSecurity(true));
          }
        }
      }
    },
    [sync, dispatch, editor]
  );

  const handleChange = (prop) => (event) => {
    if (prop) {
      setValues({ ...values, [prop]: event.target.value });
    } else if (event.target.value) {
      let aresync = Aresync.getAresync(signalingid);
      if (aresync) {
        aresync.invite({
          name: values.signalingUsername,
          target: Number.parseInt(event.target.value),
          certificates: true,
          channels: ['messaging'],
          credential: values.credential,
          username: values.username,
        });
        dispatch(setTargetid(Number.parseInt(event.target.value)));
      }
    }
  };

  const handleChecked = (prop) => (event) => {
    if (prop) {
      setValues({ ...values, [prop]: event.target.checked });
    }
  };

  const handleClickShowPassphrase = () => {
    setValues({
      ...values,
      showPassphrase: !values.showPassphrase,
    });
  };

  const handleClickShowCredential = () => {
    setValues({
      ...values,
      showCredential: !values.showCredential,
    });
  };

  const handleMouseDownPassphrase = (event) => {
    event.preventDefault();
  };

  const renderGroup = (group) => (
    group.users.map(
      (user) => {
        if (user.current) {
          return (
            <ListSubheader><Group sx={{padding: '0 4px 0 0'}} />{group.group}</ListSubheader>
          )
        }
        else {
          return (
            <MenuItem value={user.id}>
              <Tooltip title={user.id + (user.signaloss ? '#' : '@') + group.group}>
                <span>
                  {user.account ? <ManageAccounts sx={{padding: '0 8px 0 0'}} /> : <Person sx={{padding: '0 8px 0 0'}} />}
                  {user.account ? ('*' + String(user.id).slice(-4)) : user.username}
                </span>
              </Tooltip>
            </MenuItem>)
        }
      }
  ));

  const renderRemote = (id, grouplist) => {
    for (let i = 0; i < grouplist.length; i++) {
      let group = grouplist[i];
      let user = group.users.find(
        (u) => u.id === id
      );
      if (user) {
        if (user.account) {
          return (
            <Tooltip title={id + (user.signaloss ? '#' : '@') + group.group}>
              <Peers square={user.signaloss}>
                <ManageAccounts />
                <Typography variant='h6' sx={{paddingLeft: '6px'}}>{'*' + String(user.id).slice(-4)}</Typography>                
              </Peers>
            </Tooltip>
          );
        } else {
          return (
            <Tooltip title={id + (user.signaloss ? '#' : '@') + group.group}>
              <Peers square={user.signaloss}>
                <Person />
                <Typography variant='h6' sx={{paddingLeft: '6px'}}>{user.username}</Typography>                
              </Peers>
            </Tooltip>
          );
        }
      }

    }
  };

  const onconnectionstatechange = (evt) => {
    switch(evt.target.connectionState) {
      case 'connected':
        dispatch(setConnect(true));
        break;
      case 'closed':
      case 'disconnected':
      case 'failed':
        dispatch(setTargetid(0));
        dispatch(setConnect(false));
        break;
      default:
        break;
    }
  };

  const onoffer = (msg) => {
    dispatch(setTargetid(Number.parseInt(msg.id)));
  };

  const onid = (id) => {
    dispatch(setSignalingid(id));
    dispatch(setSignaling(true));
  };

  const onsignalingclose = (e) => {
    dispatch(setSignaling(false));
  };

  const onsignalingstatechange = (evt) => {
    if (evt.target.signalingState === "closed") {
      dispatch(setTargetid(0));
      dispatch(setConnect(false));
    }
  };

  const onsignalingmessage = (e) => {
    //console.dir(e);
  };

  const onuserlist = (e) => {
    e.forEach(element => {
      element.users.forEach(
        (u) => {
          if (u.current) {
            //if (u.id === signalingid) {
              if (!(values.group.length)) {
                setValues({ ...values, group: element.group });
              }
            //}
            //setUsername(u.username);
            if (u.account) {
              setAccount(true);
            }
            else {
              setAccount(false);
            }
          }
        }
      )
    });

    setGroups([...e]);
    //console.dir(e);
  };

  const closesignal = () => {
    let aresync = Aresync.getAresync(signalingid);
    if (aresync) {
      aresync.disconnect();
    }
  };

  const onclose = () => {
    let aresync = Aresync.getAresync(signalingid);
    if (aresync && targetid) {
      dispatch(setTargetid(0));
      aresync.closePeerConnection(targetid);
      dispatch(setConnect(false));
    }
  };

  const signal = async () => {
    if (((values.signaling.length > 0) || values.psi) && (values.signalingUsername.length > 0)) {
      if (!Arekey.hasLive({nanoid: editor.value})) {
        //Arekey.addPendings({nanoid: editor.value, pendings: signal});
        if (editor?.option?.crypto) {
          dispatch(openDialogPassword(true));
        } else {
          dispatch(openDialogSecurity(true));
        }
      } else {
        let option;
        const {showCredential, showPassphrase, ...others} = values;
        if (editor.option.crypto.ctu) {
          let plain = await Arekey.decrypt({nanoid: editor.value, ctu: editor.option.crypto.ctu, eku: editor.option.crypto.eku});
          option = JSON.parse(plain);
          option = {...option, sync: {service: {...others}}};
        }
        else {
          option = {sync: {service: {...others}}};
        }
        let po;
        if (values.psi && psi) {
          let fs = psi.value.split('$');
          if (fs.length === 3) {
            if (!Arekey.has({nanoid: '@psi'})) {
              Arekey.creator({nanoid: '@psi', crypto: fs[0]});
            }
            let pk = await Arekey.activate({nanoid: '@psi', passphrase: window.location.hostname});
            if (pk) {
              let plain = await Arekey.decrypt({nanoid: '@psi', eku: fs[1], ctu: fs[2]});
              po = JSON.parse(plain);
            }
          }
        }
        let crypto = await Arekey.encrypt({nanoid: editor.value, plain: JSON.stringify(option)});
        dispatch(changeEditorOption({nanoid: editor.value, option: {...editor.option, crypto: {...editor.option.crypto, eku: crypto.eku, ctu: crypto.ctu}}}));
        let configuration = {
          name: values.signalingUsername,
          events: {
            onid: onid,
            onconnectionstatechange: onconnectionstatechange,
            onsignalingstatechange: onsignalingstatechange,
            onoffer: onoffer,
            onsignalingclose: onsignalingclose,
            onsignalingmessage: onsignalingmessage,
            onuserlist: onuserlist
          },
          signaling: po ? po.signaling : values.signaling,
          stun: po ? po.stun : values.ice,
          credential: values.credential,
          username: values.username,
        };
        let aresync = Aresync.creator(configuration);
        aresync.connect({group: values.group, passphrase: values.passphrase});
      }
    }
  };

  return (
    <Box sx={{display: sync ? undefined : 'none', boxShadow: 1, height: '100%', width: '100%'}}>
      <Typography variant="h5">Sync Settings</Typography>
      <Box sx={{ display: 'flex', flexDirection: 'column' }}>
        <Box sx={{ display: 'flex', flexDirection: 'column', paddingTop: '1em' }}>
          <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'row', padding: '0 4px', justifyContent: 'space-between'}}>
            <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'row', padding: '0 4px'}}>
              <IconButton disabled={signaling || connect} onClick={signal}>
                <Link />
              </IconButton>
              <IconButton disabled={!signaling} onClick={closesignal}>
                <LinkOff />
              </IconButton>
            </Box>
            <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'row', padding: '0 4px'}}>
              <IconButton sx={{display: 'none'}}>
                <Edit />
              </IconButton>
              <Checkbox checked={values.psi} onChange={handleChecked('psi')} disabled={signaling} icon={<PublicOff />} checkedIcon={<Public />} inputProps={{ 'aria-label': 'Use default public'}} sx={{display: psi ? undefined : 'none'}} />
            </Box>
          </Box>
          { (signaling || connect) &&<Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'column', padding: '0 4px'}}>
            <Box sx={{alignItems: 'center', boxSizing: 'border-box', display: 'flex', flexDirection: 'row', padding: '2px 4px', width: '100%'}}>
              <Tooltip title={values.signalingUsername + '@' + signalingid}>
                <Peers>
                  {account ? <ManageAccounts /> : <Person />}
                  <Typography variant='h6' sx={{paddingLeft: '6px'}}>{values.signalingUsername}</Typography>
                </Peers>
              </Tooltip>
            </Box>
            { !connect && <Box sx={{boxSizing: 'border-box', display: 'flex', flexDirection: 'column', paddingTop: '1em', width: '100%'}}>
              <FormControl fullWidth size='small'>
                <InputLabel htmlFor='Invite-select'>Invite</InputLabel>
                <Select value={targetid} id='Invite-select' label="Invite" onChange={handleChange()}>
                  <MenuItem value={0}>
                    <em>None</em>
                  </MenuItem>
                  {groups.map(
                    (group) => renderGroup(group)
                  )}
                </Select>
              </FormControl>
            </Box>
            }
            { connect && <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
              <Tooltip title='Verify remote user for sync data.'>
                <span>
                  <IconButton disabled={!connect}>
                    <VerifiedUser />
                  </IconButton>
                </span>
              </Tooltip>
              {renderRemote(targetid, groups)}
              <Tooltip title='Disconnect remote peer.'>
                <span>
                  <IconButton disabled={!connect} onClick={onclose}>
                    <PersonOff />
                  </IconButton>
                </span>
              </Tooltip>
            </Box>
            }

          </Box>
          }

          { (!(signaling || connect)) && <Box sx={{ display: 'flex', flexDirection: 'column', paddingTop: '1em' }}>
            <TextField disabled={values.psi} label="Signaling" onChange={handleChange('signaling')} placeholder="wss://example.uk:8888" required size="small" sx={{ paddingBottom: '1em' }} value={values.signaling} />
            <TextField label="Use name" onChange={handleChange('signalingUsername')} required size="small" sx={{ paddingBottom: '1em' }}  value={values.signalingUsername} />
            <TextField label="Group" onChange={handleChange('group')} size="small" sx={{ paddingBottom: '1em' }}  value={values.group} />
            <FormControl variant="outlined" size="small" sx={{ paddingBottom: '1em' }}>
              <InputLabel htmlFor="signalingpassphrase">Passphrase</InputLabel>
              <OutlinedInput
                endAdornment={
                  <InputAdornment position='end'>
                    <IconButton
                      aria-label="toggle passphrase visibility"
                      edge='end'
                      onClick={handleClickShowPassphrase}
                      onMouseDown={handleMouseDownPassphrase}>
                      {values.showPassphrase ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                id="signalingpassphrase"
                label="Passphrase"
                onChange={handleChange('passphrase')}
                type={values.showPassphrase ? 'text' : 'password'}
                value={values.passphrase} />
            </FormControl>
          </Box>}

          { (!(signaling || connect)) && <Box sx={{ display: 'flex', flexDirection: 'column', paddingTop: '1em' }}>
            <TextField disabled={values.psi} label="ICE" onChange={handleChange('ice')} placeholder="stun:example.uk:9999" size="small" sx={{ paddingBottom: '1em' }} value={values.ice} />
            <TextField label="Use name" onChange={handleChange('username')} size="small" sx={{ paddingBottom: '1em' }}  value={values.username} />
            <FormControl variant="outlined" size="small" sx={{ paddingBottom: '1em' }}>
              <InputLabel htmlFor="icecredential">Credential</InputLabel>
              <OutlinedInput
                endAdornment={
                  <InputAdornment position='end'>
                    <IconButton
                      aria-label="toggle credential visibility"
                      edge='end'
                      onClick={handleClickShowCredential}
                      onMouseDown={handleMouseDownPassphrase}>
                      {values.showCredential ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                id="icecredential"
                label="Credential"
                onChange={handleChange('credential')}
                type={values.showCredential ? 'text' : 'password'}
                value={values.credential} />
            </FormControl>
          </Box>
          }

        </Box>

      </Box>
    </Box>
  );

};
