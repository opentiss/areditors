import { styled } from '@mui/material/styles';
import { Edit, Article, Folder, Work } from "@mui/icons-material";
import { TreeItem, treeItemClasses } from '@mui/x-tree-view/TreeItem';
import { Box, IconButton, Stack, TextField, Typography } from "@mui/material";
import { useState } from 'react';
import { PropTypes } from 'prop-types';
import { autoSaveToggled, fetchHybrid } from '../features/content/contentStateSlice';
import { createCategory, changeCategoryTitle, fetchCategories } from '../features/categories/categoriesSlice';
import { useDispatch, useSelector } from 'react-redux';

const StyledTreeItemRoot = styled(TreeItem)(({ theme }) => ({
    color: theme.palette.text.secondary,
    [`& .${treeItemClasses.content}`]: {
      boxSizing: 'border-box',
      color: theme.palette.text.secondary,
      fontWeight: theme.typography.fontWeightMedium,
      '&.Mui-expanded': {
        fontWeight: theme.typography.fontWeightRegular,
      },
      '&:hover': {
        backgroundColor: theme.palette.action.hover,
      },
      '&.Mui-focused, &.Mui-selected, &.Mui-selected.Mui-focused': {
        backgroundColor: `var(--tree-view-bg-color, ${theme.palette.action.selected})`,
        color: 'var(--tree-view-color)',
      },
      [`& .${treeItemClasses.label}`]: {
        fontWeight: 'inherit',
        color: 'inherit',
      },
    },
    [`& .${treeItemClasses.group}`]: {
      marginLeft: 0,
      paddingLeft: theme.spacing(2),
      [`& .${treeItemClasses.content}`]: {
        paddingLeft: theme.spacing(1),
      },
    },
  }));

export default function HybridEntry (props) {
    const {
      actionCreated,
      actionExpanded,
        labelText,
        itemId,
        children,
        incomingAction,
        journal,
        parent,
        type,
        ...other
    } = props;

    const [hovering, setHovering] = useState(false);
    const [title, setTitle] = useState(labelText);
    const [currentValue, setCurrentValue] = useState(title);
    //const [rename, setRename] = useState(false);
    const [action, setAction] = useState(incomingAction);
    const [creating, setCreating] = useState(null);
    const onMouseEnter = () => setHovering(true);
    const onMouseLeave = () => {
      setHovering(false);
      //setRename(false);
    };
    const handleChange = (e) => setCurrentValue(e.target.value);
    const dispatch = useDispatch();
    const autoSave = useSelector((state) => state?.contentState?.autoSave);
    const contentChanged = useSelector((state) => state?.contentState?.contentChanged);

    const onEdit = (e) => {
      setAction('rename');
      //setRename(true);
        console.log('onEdit:' + itemId);
        e.preventDefault();
        e.stopPropagation();
    };

    const onCreateHybrid = (e) => {
      setAction('create');
      setCreating('hybrid');
      if (actionExpanded) {
        actionExpanded(itemId);
      }
      //other.children = {{<StyledTreeItemRoot labelText='new category' nodeId='nc01'></StyledTreeItemRoot>}, ...other};
      //other = {children}.unshift(<StyledTreeItemRoot labelText='new category' nodeId='nc01'></StyledTreeItemRoot>);
      //other.push();
        console.log('onCreateHybrid:' + itemId);
        e.preventDefault();
        e.stopPropagation();
    };

    const onCreateCategory = (e) => {
      setAction('create');
      setCreating('category');
      if (actionExpanded) {
        actionExpanded(itemId);
      }
      //other.children = {{<StyledTreeItemRoot labelText='new category' nodeId='nc01'></StyledTreeItemRoot>}, ...other};
      //other = {children}.unshift(<StyledTreeItemRoot labelText='new category' nodeId='nc01'></StyledTreeItemRoot>);
      //other.push();
        console.log('onCreateCategory:' + itemId);
        e.preventDefault();
        e.stopPropagation();
    };

    const onCreateRoot = (e) => {
      setAction('create');
      setCreating('root');
        console.log('onCreateRoot:' + itemId);
        e.preventDefault();
        e.stopPropagation();
    };

    const onLoadHybrid = (e) => {
      console.log('onLoadHybrid:' + itemId);
      if (contentChanged && (!autoSave)) {
        dispatch(autoSaveToggled());
      }
      dispatch(fetchHybrid(itemId));
    };

    const handleKeyUp = async (e) => {
      // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code
      // To determine what character corresponds with the key event, use the KeyboardEvent.key property instead.
      if (e.key === 'Enter') {//e.code === 'Enter'
        if (actionCreated) {
          actionCreated({parent: parent, title: currentValue.trim(), type: type});
        }
        else {
          await dispatch(changeCategoryTitle({nanoid: itemId, title: currentValue.trim()})).then(
            (v) => {
              if (v) {
                //dispatch(fetchCategories(v?.payload?.editor));
              }
            }
          );
        }
        setTitle(currentValue.trim());
        setAction('');
        //setRename(false);
      }
      else if (e.code === 'Escape') {//e.code === 'Escape'
        //console.log(e.code);
        if (actionCreated) {
          actionCreated({cancel: true});

        }
        setCurrentValue(title);
        setAction('');
      }
    };

    const createdCategory = async (c) => {
      if (!c.cancel) {
        console.log(JSON.stringify(c));
        await dispatch(createCategory(c)).then(
          (v) => {
            console.log(JSON.stringify(v.payload));
            dispatch(fetchCategories(v?.payload?.editor));
          }
        );  
      }
      //await dispatch(fetchCategories());
      setAction('');
      setCreating(null);
    }

    return (
        <StyledTreeItemRoot
        itemId={itemId}
        sx={{ boxSizing: 'border-box'}}
        label={
          <Box
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            onClick={(type === 'hybrid') ? onLoadHybrid : undefined}
            sx={{ display: 'flex', alignItems: 'center', flexGrow: 1, boxSizing: 'border-box', padding: 0}}>
              <Box
              component={type === 'root' ? Work :
              type === 'category' ? Folder :
              Article} sx={{mr: 1}} />
              {action === 'rename' ? <TextField
              onChange={handleChange}
              onKeyDown={handleKeyUp}
              autoFocus={true}
              value={currentValue}
              type='text'
              size="small"
              sx={{width: '100%'}}
              variant='standard'/>
              :
              <Typography style={{boxSizing: 'border-box', textOverflow: 'clip', whiteSpace: 'nowrap', overflow: 'hidden', padding: 0}} variant="body1" sx={{ flexGrow: 1, boxSizing: 'border-box', display: { xs: 'none', sm: 'block' } }}>
                {title}
              </Typography>
              }
              {!action && <Stack direction='row' style={{right: '0rem', position: 'absolute'}}>{hovering &&
                <>
                <IconButton onClick={onEdit} sx={{padding: 0}}><Edit></Edit></IconButton>
                {(type !== 'hybrid' && journal !== 'journal') && (
                  <>
                  <IconButton onClick={onCreateHybrid} sx={{padding: 0}}><Article></Article></IconButton>
                  <IconButton onClick={onCreateCategory} sx={{padding: 0}}><Folder></Folder></IconButton>
                  {type === 'root' && <IconButton onClick={onCreateRoot} sx={{padding: 0}}><Work></Work></IconButton>}
                  </>
                  )}
                </>
                }</Stack>
              }
            </Box>
        }
        {...other}>
          {children ? (<>
            {creating !== null && <HybridEntry actionCreated={createdCategory} incomingAction='rename' labelText={creating === 'root' ? labelText + ' sibling' : 'Child ' + creating} itemId={itemId + creating} parent={itemId} type={creating} />}
            {children}
          </>)
          :
            creating !== null && <HybridEntry actionCreated={createdCategory} incomingAction='rename' labelText={creating === 'root' ? labelText + ' sibling' : 'Child ' + creating} itemId={itemId + creating} parent={itemId} type={creating} />
          }
        </StyledTreeItemRoot>
    )
};

HybridEntry.propTypes = {
  actionCreated: PropTypes.func,
  actionExpanded: PropTypes.func,
  actionSelected: PropTypes.func,
  journal: PropTypes.string,
  incomingAction: PropTypes.string,
  labelText: PropTypes.string.isRequired,
  parent: PropTypes.string,
  tag: PropTypes.string,
  type: PropTypes.string.isRequired,
};
