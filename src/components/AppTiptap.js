import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTheme } from "@mui/material";
import { autoSaveToggled, changeHybrid, contentChangedToggled, decryptHybrid, setHybridPending, timeoutIDUpdate } from '../features/content/contentStateSlice';
import { openDialogPassword } from "../features/action/actionStateSlice";
import Arekey from "../models/Arekey";
import { EditorState } from "@tiptap/pm/state";
import { Node } from "@tiptap/pm/model";
import { Blockquote } from "@tiptap/extension-blockquote";
import { Bold } from "@tiptap/extension-bold";
import { BulletList } from "@tiptap/extension-bullet-list";
import { Code } from "@tiptap/extension-code";
import { CodeBlock } from "@tiptap/extension-code-block";
import { Color } from "@tiptap/extension-color";
import { Document } from "@tiptap/extension-document";
import { Dropcursor } from "@tiptap/extension-dropcursor";
import { FontFamily } from "@tiptap/extension-font-family";
import { Gapcursor } from "@tiptap/extension-gapcursor";
import { HardBreak } from "@tiptap/extension-hard-break";
import { Highlight } from "@tiptap/extension-highlight";
import { History } from "@tiptap/extension-history";
import { HorizontalRule } from "@tiptap/extension-horizontal-rule";
import { Italic } from "@tiptap/extension-italic";
import { Link } from "@tiptap/extension-link";
import { ListItem } from "@tiptap/extension-list-item";
import { OrderedList } from "@tiptap/extension-ordered-list";
import { Paragraph } from "@tiptap/extension-paragraph";
import { Placeholder } from "@tiptap/extension-placeholder";
import { Strike } from "@tiptap/extension-strike";
import { Subscript } from "@tiptap/extension-subscript";
import { Superscript } from "@tiptap/extension-superscript";
import { TableCell } from "@tiptap/extension-table-cell";
import { TableHeader } from "@tiptap/extension-table-header";
import { TableRow } from "@tiptap/extension-table-row";
import { TaskItem } from "@tiptap/extension-task-item";
import { TaskList } from "@tiptap/extension-task-list";
import { Text } from "@tiptap/extension-text";
import { TextAlign } from "@tiptap/extension-text-align";
import { TextStyle } from "@tiptap/extension-text-style";
import { Underline } from "@tiptap/extension-underline";
import {
  FontSize,
  HeadingWithAnchor,
  LinkBubbleMenu,
  LinkBubbleMenuHandler,
  MenuButtonAddTable,
  MenuButtonBlockquote,
  MenuButtonBold,
  MenuButtonBulletedList,
  MenuButtonCode,
  MenuButtonCodeBlock,
  MenuButtonEditLink,
  MenuButtonHighlightColor,
  MenuButtonHorizontalRule,
  MenuButtonImageUpload,
  MenuButtonIndent,
  MenuButtonItalic,
  MenuButtonOrderedList,
  MenuButtonRedo,
  MenuButtonRemoveFormatting,
  MenuButtonStrikethrough,
  MenuButtonSubscript,
  MenuButtonSuperscript,
  MenuButtonTaskList,
  MenuButtonTextColor,
  MenuButtonUnderline,
  MenuButtonUndo,
  MenuButtonUnindent,
  MenuControlsContainer,
  MenuDivider,
  MenuSelectFontFamily,
  MenuSelectFontSize,
  MenuSelectHeading,
  MenuSelectTextAlign,
  ResizableImage,
  RichTextEditor,
  TableBubbleMenu,
  TableImproved,
  isTouchDevice,
} from "mui-tiptap";

const CustomLinkExtension = Link.extend({
  inclusive: false,
});

const CustomSubscript = Subscript.extend({
  excludes: "superscript",
});

const CustomSuperscript = Superscript.extend({
  excludes: "subscript",
});

const extensions = [
  // We use some but not all of the extensions from
  // https://tiptap.dev/api/extensions/starter-kit, plus a few additional ones

  // Note that the Table extension must come before other nodes. See README
  TableImproved.configure({
    resizable: true,
  }),
  TableRow,
  TableHeader,
  TableCell,

  BulletList,
  CodeBlock,
  Document,
  HardBreak,
  ListItem,
  OrderedList,
  Paragraph,
  CustomSubscript,
  CustomSuperscript,
  HorizontalRule,
  Text,
  Color,
  FontFamily,
  FontSize,
  Highlight.configure({ multicolor: true }),
  TextAlign.configure({
    types: ["heading", "paragraph", "image"],
  }),
  TextStyle,
  // Blockquote must come after Bold, since we want the "Cmd+B" shortcut to
  // have lower precedence than the Blockquote "Cmd+Shift+B" shortcut. See
  // README
  Bold,
  Blockquote,

  Code,
  Italic,
  Strike,
  CustomLinkExtension.configure({
    autolink: true,
    linkOnPaste: true,
    openOnClick: false,
  }),
  LinkBubbleMenuHandler,

  // Extensions
  Gapcursor,
  HeadingWithAnchor.configure({
    // People shouldn't typically need more than 3 levels of headings, so
    // keep a more minimal set (than the default 6) to keep things simpler
    // and less chaotic.
    levels: [1, 2, 3],
  }),

  ResizableImage,

  // When images are dragged, we want to show the "drop cursor" for where they'll
  // land
  Dropcursor,

  TaskList,
  TaskItem.configure({
    nested: true,
  }),

  Placeholder.configure({
    placeholder: "Add your own content here...",
  }),
  Underline,
  // We use the regular `History` (undo/redo) extension when not using
  // collaborative editing
  History,
];

const currentEditorState = {time: 0, changed: false};

function TEditor() {
  const rteRef = useRef(null);
  const theme = useTheme();
  // const extensions = [StarterKit];
  const dispatch = useDispatch();

  // const [editorState, setEditorState] = useState(undefined);
  const editor = useSelector(state => state?.activityBar?.actions?.editor);
  const hybrid = useSelector((state) => state?.contentState?.hybrid);
  const autoSave = useSelector((state) => state?.contentState?.autoSave);
  const contentChanged = useSelector((state) => state?.contentState?.contentChanged);
  //const key = useSelector((state) => state?.options?.editor?.key);
  const timeoutID = useSelector((state) => state?.contentState?.timeoutID);
  // const [undoSize, setUndoSize] = useState(0);
  // const [startOffset, setStartOffset] = useState(undefined);
  const onChanged = ({ editor, transaction }) => {
    if (transaction.docChanged && transaction.updated) {
      if (currentEditorState.time !== transaction.time) {
        if ((!(currentEditorState.changed)) && (!contentChanged)) {
          currentEditorState.changed = !contentChanged;
          dispatch(contentChangedToggled());
          // console.log(editorState.getLastChangeType());
          const tid = setTimeout(() => {
            if (!autoSave) {
              currentEditorState.changed = false;
              dispatch(autoSaveToggled());
            }
          }, 30000);
          dispatch(timeoutIDUpdate(tid));
          console.log(Date.now(), 'onTransaction', transaction.time, transaction.docChanged); // eslint-disable-line no-console
        }
        currentEditorState.time = transaction.time;
        // The content has changed.
        // rteRef.current.editor.getJSON();
      }
    }
  };

  useEffect(
    () => {
      if (contentChanged && (!editor) && (!autoSave)) {
        dispatch(autoSaveToggled());
        currentEditorState.changed = false;
      }
      currentEditorState.changed = contentChanged;
    },
    [autoSave, contentChanged, dispatch, editor]
  );

  useEffect (
    () => {
      if (hybrid && (hybrid.loading)) {
        // rteRef.current.editor.commands.clearContent();
        let contentJSON;
        if(hybrid?.data) {
          if (hybrid?.option?.crypto && hybrid?.data?.crypto?.ctu) {
            if (!Arekey.hasLive({nanoid: hybrid.editor})) {
              dispatch(setHybridPending({...hybrid, decrypt: true}));
              dispatch(openDialogPassword(true));
            }
            else {
              dispatch(decryptHybrid(hybrid));
            }
          }
          else {
            // setEditorState(EditorState.createWithContent(convertFromRaw(hybrid.data)));
            // console.log(convertFromRaw(hybrid.data).toString());
            if (rteRef.current.editor) {
              if ((hybrid?.mimetype === 'application/draft-js-raw+json') && hybrid.data?.blocks?.length) {
                const content = {type: 'doc', content: hybrid.data?.blocks.map(_b => {
                  const {text} = _b;
                  if (text?.length) {
                    return {type: 'paragraph', content: [{type: 'text', text}]};
                  } else {
                    return {type: 'paragraph'};
                  }
                })};
                // rteRef.current.editor.commands.setContent(content);
                contentJSON = content;
              } else if (hybrid?.mimetype === 'application/prosemirror+json') {
                // rteRef.current.editor.commands.setContent(hybrid.data);
                contentJSON = hybrid.data;
              }
            }
          }
        }
        else {
          if (hybrid.title) {
            // rteRef.current.editor.commands.setContent({type: 'doc', content: [{type: 'paragraph', content: [{type: 'text', text: hybrid.title}]}]});// setEditorState(EditorState.createWithText(hybrid.title));
            contentJSON = {type: 'doc', content: [{type: 'paragraph', content: [{type: 'text', text: hybrid.title}]}]};
          } else {
            // rteRef.current.editor.commands.setContent({type: 'doc', content: [{type: 'paragraph'}]});// setEditorState(EditorState.createWithText(hybrid.title));
            contentJSON = {type: 'doc', content: [{type: 'paragraph'}]};
          }
        }
        if (contentJSON) {
          rteRef.current.editor.view.updateState(EditorState.create({
            doc: Node.fromJSON(rteRef.current.editor.extensionManager.schema, contentJSON),
            schema: rteRef.current.editor.extensionManager.schema,
            plugins: rteRef.current.editor.extensionManager.plugins,
          }))
        }

        rteRef.current.editor.on('transaction', onChanged);
        // currentEditorState.changed = false;
        // rteRef.current.editor.on('transaction', ({ editor, transaction }) => {
        //   if (transaction.docChanged && transaction.updated) {
        //     if (currentEditorState.time !== transaction.time) {
        //       if (currentEditorState.time && (!(currentEditorState.changed)) && (!contentChanged)) {
        //         currentEditorState.changed = !contentChanged;
        //         dispatch(contentChangedToggled());
        //         // console.log(editorState.getLastChangeType());
        //         const tid = setTimeout(() => {
        //           if (!autoSave) {
        //             currentEditorState.changed = false;
        //             dispatch(autoSaveToggled());
        //           }
        //         }, 30000);
        //         dispatch(timeoutIDUpdate(tid));
        //         console.log(Date.now(), 'onTransaction', transaction.time, transaction.docChanged); // eslint-disable-line no-console
        //       }
        //       currentEditorState.time = transaction.time;
        //       // The content has changed.
        //       // rteRef.current.editor.getJSON();
        //     }
        //   }
        // });
      }

      return () => {
        if (currentEditorState.time) {
          // console.log(JSON.stringify(currentEditorState));
          currentEditorState.time = 0;
        }
      }
    },
    [dispatch, hybrid]
  );

  useEffect(() => {
    if (autoSave && contentChanged ) {
      const raw = rteRef.current.editor.getJSON();//convertToRaw(editorState.getCurrentContent());
      const mimetype = 'application/prosemirror+json';
      if (timeoutID) {
        clearTimeout(timeoutID); //dispatch(timeoutIDUpdate(undefined));
      }

      if (hybrid?.option?.crypto) {
        // First, check and clear saved master key after reached editor option lifetime.
        if (!Arekey.hasLive({nanoid: hybrid.editor})) {//!key
          if (hybrid.mimetype !== mimetype) {
            dispatch(setHybridPending({...hybrid, data: raw, mimetypechange: mimetype}));
          }
          else {
            dispatch(setHybridPending({...hybrid, data: raw}));
          }
          dispatch(openDialogPassword(true));
        }
        else {
          //dispatch(changeHybrid({...hybrid, data: raw, mimetypechange: 'application/draft-js-raw+json', master: key}));
          if (hybrid.mimetype !== mimetype) {
            dispatch(changeHybrid({...hybrid, data: raw, mimetypechange: mimetype}));//, master: key
          }
          else {
            dispatch(changeHybrid({...hybrid, data: raw}));//, master: key
          }
        }
      }
      else {
        if (hybrid.mimetype !== mimetype) {
          dispatch(changeHybrid({...hybrid, data: raw, mimetypechange: mimetype}));
        }
        else {
          dispatch(changeHybrid({...hybrid, data: raw}));
        }
      }
    }
  }, [autoSave, contentChanged, dispatch, hybrid, timeoutID]);

  return (
    <div>
      <RichTextEditor
        ref={rteRef}
        extensions={extensions}
        renderControls={() => (
          <MenuControlsContainer>
            <MenuSelectFontFamily
              options={[
                { label: "Comic Sans", value: "Comic Sans MS, Comic Sans" },
                { label: "Cursive", value: "cursive" },
                { label: "Monospace", value: "monospace" },
                { label: "Serif", value: "serif" },
              ]}
            />
            <MenuSelectFontSize />
            <MenuButtonTextColor
              defaultTextColor={theme.palette.text.primary}
              swatchColors={[
                { value: "#000000", label: "Black" },
                { value: "#ffffff", label: "White" },
                { value: "#888888", label: "Grey" },
                { value: "#ff0000", label: "Red" },
                { value: "#ff9900", label: "Orange" },
                { value: "#ffff00", label: "Yellow" },
                { value: "#00d000", label: "Green" },
                { value: "#0000ff", label: "Blue" },
              ]}
            />
            <MenuButtonHighlightColor
              swatchColors={[
                { value: "#595959", label: "Dark grey" },
                { value: "#dddddd", label: "Light grey" },
                { value: "#ffa6a6", label: "Light red" },
                { value: "#ffd699", label: "Light orange" },
                // Plain yellow matches the browser default `mark` like when using Cmd+Shift+H
                { value: "#ffff00", label: "Yellow" },
                { value: "#99cc99", label: "Light green" },
                { value: "#90c6ff", label: "Light blue" },
                { value: "#8085e9", label: "Light purple" },
              ]}
            />

            <MenuDivider />

            <MenuSelectHeading />
            <MenuSelectTextAlign />

            <MenuDivider />

            <MenuButtonBold />
            <MenuButtonItalic />
            <MenuButtonUnderline />
            <MenuButtonStrikethrough />
            <MenuButtonSubscript />
            <MenuButtonSuperscript />

            <MenuDivider />

            <MenuButtonEditLink />

            <MenuDivider />

            <MenuButtonOrderedList />
            <MenuButtonBulletedList />
            <MenuButtonTaskList />

            <MenuDivider />

            {/* On touch devices, we'll show indent/unindent buttons, since they're
            unlikely to have a keyboard that will allow for using Tab/Shift+Tab. These
            buttons probably aren't necessary for keyboard users and would add extra
            clutter. */}
            {isTouchDevice() && (
              <>
                <MenuButtonIndent />

                <MenuButtonUnindent />
              </>
            )}
            <MenuButtonBlockquote />

            <MenuDivider />

            <MenuButtonCode />

            <MenuButtonCodeBlock />

            <MenuDivider />

            <MenuButtonImageUpload
              onUploadFiles={(files) =>
                // For the sake of a demo, we don't have a server to upload the files
                // to, so we'll instead convert each one to a local "temporary" object
                // URL. This will not persist properly in a production setting. You
                // should instead upload the image files to your server, or perhaps
                // convert the images to bas64 if you would like to encode the image
                // data directly into the editor content, though that can make the
                // editor content very large.
                files.map((file) => ({
                  src: URL.createObjectURL(file),
                  alt: file.name,
                }))
              }
            />

            <MenuDivider />

            <MenuButtonHorizontalRule />

            <MenuButtonAddTable />

            <MenuDivider />

            <MenuButtonRemoveFormatting />

            <MenuDivider />

            <MenuButtonUndo />
            <MenuButtonRedo />

          </MenuControlsContainer>
        )}
      >
        {() => (
          <>
            <LinkBubbleMenu />
            <TableBubbleMenu />
          </>
        )}
      </RichTextEditor>
    </div>
  );
}

export default TEditor;
