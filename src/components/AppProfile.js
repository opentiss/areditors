import { useEffect, useState } from "react";
import { Button, Box, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { openDialogProfile } from "../features/action/actionStateSlice";
import { changeEditorTitle } from "../features/options/optionsSlice";

export default function AppProfile(props) {
    const dispatch = useDispatch();
    const dialogProfile = useSelector((state) => state?.action?.actionProfile?.dialog);
    const editor = useSelector((state) => state?.options?.editor);

    const handleCancel = () => {
        if (dialogProfile) {
            setValues({
                disabled: false,
                title: editor?.title,
            });
            dispatch(openDialogProfile(false));
        }
    };

    const handleOk = async () => {
        if (dialogProfile && values?.title?.length && (values.title !== editor.title)) {
            setValues({ ...values, disabled: true });
            dispatch(changeEditorTitle({nanoid: editor.value, title: values.title}));
            setValues({ ...values, disabled: false });
            dispatch(openDialogProfile(false));
        }
    };

    const [values, setValues] = useState({
        disabled: false,
        title: editor?.title,
    });

    useEffect (
        () => setValues({ title: editor?.title }),
        [editor?.title]
    );

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    return (
        <Dialog open={dialogProfile}>
            <DialogTitle>Profile</DialogTitle>
            <DialogContent dividers>
                <DialogContentText>
                    Edit current editor title.
                </DialogContentText>
                <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                <TextField
                    label='Title'
                    value={values.title}
                    onChange={handleChange('title')}
                    sx={{m: 1, width: '64ch'}}
                />

                </Box>

            </DialogContent>
            <DialogActions>
                <Button disabled={values.disabled} autoFocus onClick={handleCancel}>
                    Close
                </Button>
                <Button disabled={values.disabled} autoFocus onClick={handleOk}>
                    Save
                </Button>

            </DialogActions>
        </Dialog>
    );
}
