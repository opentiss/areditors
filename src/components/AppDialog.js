import AppImportExport from "./AppImportExport";
import AppPassword from "./AppPassword";
import AppProfile from "./AppProfile";
import AppSecurity from "./AppSecurity";

export default function AppDialog(props) {

    return (
        <>
            <AppImportExport></AppImportExport>
            <AppPassword></AppPassword>
            <AppProfile></AppProfile>
            <AppSecurity></AppSecurity>
        </>
    );
}
