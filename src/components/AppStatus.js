import { Divider, Paper, Stack, styled } from "@mui/material";
import { useSelector } from 'react-redux';

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: '0 1em',
  textAlign: 'center',
}));

export default function AppStatus (props) {
  const hybrid = useSelector((state) => state?.contentState?.hybrid);

  return (
    <Stack
    direction='row'
    divider={<Divider orientation="vertical" flexItem />}>
      <Item>{hybrid?.title}</Item>
      <Item>{hybrid?.mimetype}</Item>
    </Stack>

  )
}
