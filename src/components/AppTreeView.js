import { useState } from 'react';
import { useSelector } from 'react-redux';
import { SimpleTreeView } from '@mui/x-tree-view/SimpleTreeView';
import HybridEntry from './HybridEntry';

export default function AppTreeView(props) {
  const data = useSelector((state) => state?.categories?.entities);
  const editor = useSelector(state => state?.activityBar?.actions?.editor);

    const [expanded, setExpanded] = useState([]);
    const [selected, setSelected] = useState('');

    const actionExpanded = (n) => {
      let i = expanded.indexOf(n);
      if (i === -1) {
        setExpanded(expanded.concat([n]));
      }
    }

    // const actionSelected = (n) => {
    //   setSelected(n);
    // }

    const onNodeSelect = (e, n) => {
      //console.log(JSON.stringify(e));
      //console.log(JSON.stringify(n));
      setSelected(n);
    }

    const onNodeToggle = (e, n) => {
      //console.log(JSON.stringify(e));
      //console.log(JSON.stringify(n));
      setExpanded(n);
    }

    const renderTree = (node) => (
      <HybridEntry key={node.nanoid} itemId={node.nanoid} labelText={node.title} parent={node.parent} tag={node.tag} type={node.type} journal={node.journal} actionExpanded={actionExpanded}>
        {Array.isArray(node.children) ? node.children.map((c) => renderTree(c)) : null}
      </HybridEntry>
    )

    if (Array.isArray(data)) {
      return (
        <SimpleTreeView
        multiSelect={false}
        expanded={expanded}
        selected={selected}
        onItemClick={onNodeSelect}
        onItemSelectionToggle={onNodeToggle}
        sx={{display: editor ? undefined : 'none', overflow: 'visible', position: 'relative', boxSizing: 'border-box', margin: 0}}
        >
          {data && data.map(node => renderTree(node))}
        </SimpleTreeView>
      );

    }
};
