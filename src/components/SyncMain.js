import { ContentCopy, Send } from "@mui/icons-material";
import { Box, Divider, FormControl, IconButton, InputLabel, OutlinedInput, Tooltip } from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from 'react-redux';
import Aresync from "../models/Aresync";

export default function SyncMain(props) {

  const sync = useSelector(state => state?.activityBar?.actions?.sync);
  const signalingid = useSelector(state => state?.action?.actionSync?.signalingid);
  const targetid = useSelector(state => state?.action?.actionSync?.targetid);
  const connect = useSelector(state => state?.action?.actionSync?.connect);
  const [placeholder, setPlaceholder] = useState('Messaging');
  const [messaging, setMessaging] = useState('');
  const [receiving, setReceiving] = useState('');
  const [canSend, setCanSend] = useState(false);
  const [canCopy, setCanCopy] = useState(false);

  const change = (e) => {
    if (e.target.value && e.target.value.length) {
      if (!canSend) {
        setCanSend(true);
      }
    }
    else {
      if (canSend) {
        setCanSend(false);
      }
    }
    setMessaging(e.target.value);
  };

  const onmessage = (evt) => {
    if (evt.channel === 'messaging') {
      let msg = JSON.parse(evt.data);
      setReceiving(msg.text);
      setCanCopy(true);
    }
  };
  const onSend = (e) => {
    send();
    //setSending(true);
  };

  function send () {
    if(signalingid && targetid) {
      let aresync = Aresync.getAresync(signalingid);
      if (aresync) {
        let msg = {
          text: messaging,
          type: "message",
          id: signalingid,
          name: 'clientUsername',
          date: Date.now()
        };
        aresync.send({channel: 'messaging', target: targetid, data: JSON.stringify(msg)});
        setPlaceholder(messaging);
        setMessaging('');
        setCanSend(false);
        // setSending(false);
      }
    }
  };

  const handleKeyUp = async (evt) => {
    // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code
    // To determine what character corresponds with the key event, use the KeyboardEvent.key property instead.
    if (evt.key === 'Enter') {//e.code === 'Enter'
      if (evt.ctrlKey) {
        send();
        //setSending(true);
      }
    }
    else if (evt.code === 'Escape') {//e.code === 'Escape'
    }
  };

  const copyreceived = (evt) => {
    navigator.permissions.query({ name: 'clipboard-read' }).then(
      (result) => {
        if (result.state === 'granted' || result.state === 'prompt') {
          navigator.clipboard.readText().then(
            () => {
              const copied = document.getElementById("received").value;
              //console.log(Date.now() + 'copied.' + copied);
              navigator.clipboard.writeText(copied).then(() => {
                setCanCopy(false);
                setTimeout(
                  (text) => {
                    setCanCopy(true);
                    //console.log(Date.now() + 'text.' + text);
                    navigator.clipboard.readText().then(
                      (clipText) => {
                        if (text === clipText) {
                          navigator.clipboard.writeText('');
                          //console.log(Date.now() + 'clear.' + copied);
                        }    
                      }
                    );
                  },
                  15000,
                  copied
                );
              });
    
            }
          );
        }
      }
    );
  }

  // const requstSecure = () => {
  //   if (!Arekey.has({nanoid: editor.value})) {
  //     if (editor?.option?.crypto) {
  //       dispatch(openDialogPassword(true));
  //     } else {
  //       dispatch(openDialogSecurity(true));
  //     }
  //   }
  // };

  useEffect(
    () => {
      if (signalingid) {
        let aresync = Aresync.getAresync(signalingid);
        if (aresync) {
          aresync.addEvent('onmessage', onmessage);
        }
      }
    },
    [signalingid]
  );
  
  // useEffect(
  //   () => {
  //     if (sending) {
  //     }
  //   },
  //   [sending]
  // );

  return (
    <Box sx={{display: sync ? undefined : 'none', boxShadow: 1, height: '100%', width: '100%'}}>
      <Box sx={{ boxSizing: 'border-box', width: '100%', display: connect ? 'flex' : 'none', flexDirection: 'column', padding: '0.5em 4px 0'}}>
        <FormControl fullWidth>
          <OutlinedInput id="received" multiline readOnly rows={4} value={receiving} />
        </FormControl>
        <IconButton disabled={!canCopy} onClick={copyreceived} sx={{top: '0.75rem', right: '0.75rem', position: 'absolute'}}><ContentCopy /></IconButton>
        <Divider sx={{height: '1em'}} />
        <FormControl fullWidth>
          <InputLabel htmlFor="messaging">Messaging</InputLabel>
          <OutlinedInput id="messaging" onKeyUp={handleKeyUp} placeholder={placeholder} label="Messaging" multiline onChange={change} rows={4} value={messaging} />
        </FormControl>
        <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
          <Tooltip title='Send text to remote peer.'>
            <span>
              <IconButton disabled={!canSend} onClick={onSend}>
                <Send />
              </IconButton>
            </span>
          </Tooltip>
        </Box>

      </Box>
    </Box>
  );

};
