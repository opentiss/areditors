import { useState } from "react";
import { Button, Box, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, FormHelperText, IconButton, InputAdornment, InputLabel, OutlinedInput } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { openDialogImportExport } from "../features/action/actionStateSlice";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { exportEditor } from "../models/synchronize";

export default function AppImportExport(props) {
    const dispatch = useDispatch();
    const dialogImportExport = useSelector((state) => state?.action?.actionImportExport?.dialog);
    const resetValues = () => {
        setValues({
            disabled: false,
            password: '',
            verifypassword: '',
            verifyerror: '',
            showPassword: false,
            showVerifyPassword: false,
            export: false,
        });
    }
    const handleCancel = () => {
        if (dialogImportExport) {
            resetValues();
            dispatch(openDialogImportExport(false));
        }
    };

    const handleOk = async () => {
        if (dialogImportExport
            && (values.password.length > 0)
            && (values.password === values.verifypassword)) {
            setValues({ ...values, disabled: true });
            await exportEditor(values.password);
            resetValues();
            dispatch(openDialogImportExport(false));
        }
        else {
            setValues({ ...values, verifyerror: 'Verify password fail.' });
        }
    };

    const [values, setValues] = useState({
        disabled: false,
        password: '',
        verifypassword: '',
        showPassword: false,
        showVerifyPassword: false,
        export: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
    };

    const handleClickShowVerifyPassword = () => {
        setValues({
          ...values,
          showVerifyPassword: !values.showVerifyPassword,
        });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    return (
        <Dialog open={dialogImportExport}>
            <DialogTitle>Import Export Dexie</DialogTitle>
            <DialogContent dividers>
                <DialogContentText>
                    Import or Export current editor data stored in Dexie.
                </DialogContentText>
                <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                <FormControl required sx={{m: 1, width: '64ch'}} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password-ie">Password for zip file</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-password-ie"
                        type={values.showPassword ? 'text' : 'password'}
                        value={values.password}
                        onChange={handleChange('password')}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge='end'>
                                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                        label="Password for zip file"
                    />
                </FormControl>
                <FormControl required sx={{m: 1, width: '64ch'}} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password-iev">Verify password for zip file</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-password-iev"
                        type={values.showVerifyPassword ? 'text' : 'password'}
                        value={values.verifypassword}
                        onChange={handleChange('verifypassword')}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowVerifyPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge='end'>
                                    {values.showVerifyPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                        label="Verify password for zip file"
                        aria-errormessage={values.verifyerror}
                        error={values.verifyerror !== ''}
                    />
                    <FormHelperText error={values.verifyerror !== ''}>{values.verifyerror}</FormHelperText>
                </FormControl>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button disabled={values.disabled} autoFocus onClick={handleCancel}>
                    Close
                </Button>
                <Button disabled={values.export} onClick={handleOk}>
                    Export
                </Button>
            </DialogActions>
        </Dialog>
    );
}
