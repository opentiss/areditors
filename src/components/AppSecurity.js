import { useState } from "react";
import { Button, Box, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, FormHelperText, IconButton, InputAdornment, InputLabel, OutlinedInput } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { openDialogSecurity } from "../features/action/actionStateSlice";
import { changeEditorOption } from "../features/options/optionsSlice";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import Arekey from "../models/Arekey";

export default function AppSecurity(props) {
    const dispatch = useDispatch();
    const dialogSecurity = useSelector((state) => state?.action?.actionSecurity?.dialog);
    const editor = useSelector((state) => state?.options?.editor);

    const handleCancel = () => {
        if (dialogSecurity) {
            resetValues();
            dispatch(openDialogSecurity(false));
        }
    };

    const resetValues = () => {
        setValues({
            disabled: false,
            password: '',
            newpassword: '',
            verifypassword: '',
            showPassword: false,
            showNewPassword: false,
            showVerifyPassword: false,
            execute: false,
        });
    };

    const handleDeriveKey = async () => {
        if (dialogSecurity) {
            if ((values.password.length > 0)
                && (values.password === values.verifypassword)) {
                setValues({ ...values, disabled: true, execute: true });
                let k = await Arekey.generate({nanoid: editor.value, passphrase: values.password})
                if (!editor?.option?.crypto) {
                    dispatch(changeEditorOption({nanoid: editor.value, option: {...editor.option, crypto: {pku: k.crypto}}}));
                }
                resetValues();
                dispatch(openDialogSecurity(false));
            }
            else {
                setValues({ ...values, verifyerror: 'Verify password fail.' });
            }
        }
    };

    const handleChangePassword = async () => {
        if (dialogSecurity) {
            // // Test spread.
            // let s = {o: {e: {t: 'title', s: Date.now(), k: {s: 'BASE64URL', a: {iv: 'random'}}}, h: {d: 'plaintext'}}};
            // const sc = {...s, o: {...s.o, e: {...s.o.e, k: 'updated'}}};
            // console.log(JSON.stringify(sc));
            if ((values.password.length > 0)
                && (values.newpassword.length > 0)
                && (values.newpassword !== values.password)
                && (values.newpassword === values.verifypassword)) {
                setValues({ ...values, disabled: true, execute: true });
                let k;// = await changePBK(values.password, editor.option.crypto.pku, values.newpassword);
                k = await Arekey.update({nanoid: editor.value, crypto: editor.option.crypto.pku, passphrase: values.password, newpassphrase: values.newpassword});
                if (!k.fail) {
                    dispatch(changeEditorOption({nanoid: editor.value, option: {...editor.option, crypto: {pku: k.crypto}}}));
                    resetValues();
                    dispatch(openDialogSecurity(false));
                }
                else {
                    setValues({ ...values, verifyerror: 'Old password incorrect.' });
                }
            }
            else {
                setValues({ ...values, verifyerror: 'Verify input password fail.' });
            }
        }
    };

    const [values, setValues] = useState({
        disabled: false,
        password: '',
        newpassword: '',
        verifypassword: '',
        showPassword: false,
        showNewPassword: false,
        showVerifyPassword: false,
        execute: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
    };

    const handleClickShowVerifyPassword = () => {
        setValues({
          ...values,
          showVerifyPassword: !values.showVerifyPassword,
        });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    if (editor) {
        return (
            <Dialog open={dialogSecurity}>
                <DialogTitle>Security</DialogTitle>
                <DialogContent dividers>
                    <DialogContentText>
                        Generate current editor master key protected with password.
                    </DialogContentText>
                    <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                    <FormControl required sx={{m: 1, width: '64ch'}} variant="outlined">
                    {editor?.option?.crypto ? <InputLabel htmlFor="outlined-adornment-password-pbk">Old password to protect master key</InputLabel> : <InputLabel htmlFor="outlined-adornment-password-pbk">Password to protect master key</InputLabel>}
                        
                        <OutlinedInput
                            id="outlined-adornment-password-pbk"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.password}
                            onChange={handleChange('password')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge='end'>
                                        {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label={editor?.option?.crypto ? 'Old password to protect master key' : 'Password to protect master key'}
                        />
                    </FormControl>
                    {editor?.option?.crypto && <FormControl required sx={{m: 1, width: '64ch'}} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password-pbkn">New password to protect master key</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password-pbkn"
                            type={values.showNewPassword ? 'text' : 'password'}
                            value={values.newpassword}
                            onChange={handleChange('newpassword')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge='end'>
                                        {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="New password to protect master key"
                        />
                    </FormControl>}
                    <FormControl required sx={{m: 1, width: '64ch'}} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password-pbkv">Verify password</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password-pbkv"
                            type={values.showVerifyPassword ? 'text' : 'password'}
                            value={values.verifypassword}
                            onChange={handleChange('verifypassword')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowVerifyPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge='end'>
                                        {values.showVerifyPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="Verify password"
                            aria-errormessage={values.verifyerror}
                            error={values.verifyerror !== ''}
                        />
                        <FormHelperText error={values.verifyerror !== ''}>{values.verifyerror}</FormHelperText>
                    </FormControl>
    
                    </Box>
    
                </DialogContent>
                <DialogActions>
                    <Button disabled={values.disabled} autoFocus onClick={handleCancel}>
                        Close
                    </Button>
                    {(!editor?.option?.crypto) && <Button disabled={values.execute} onClick={handleDeriveKey}>
                        DeriveKey
                    </Button>}
                    {editor?.option?.crypto && <Button disabled={values.execute} onClick={handleChangePassword}>
                        ChangePassword
                    </Button>}
                </DialogActions>
            </Dialog>
        );
    
    }

}
