import { useState } from "react";
import { Button, Box, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, FormHelperText, IconButton, InputAdornment, InputLabel, OutlinedInput } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { openDialogPassword } from "../features/action/actionStateSlice";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { changeHybrid, decryptHybrid } from "../features/content/contentStateSlice";
import Arekey from "../models/Arekey";

export default function AppPassword(props) {
    const dispatch = useDispatch();
    const dialogPassword = useSelector((state) => state?.action?.actionPassword?.dialog);
    const editor = useSelector((state) => state?.options?.editor);
    const hybridPending = useSelector((state) => state?.contentState?.hybridPending)
    // const hybrid = useSelector((state) => state?.contentState?.hybrid)

    const resetValues = () => {
        setValues({
            disabled: false,
            password: '',
            showPassword: false,
            verifyerror: '',
            execute: false,
        });
    };

    const handleChangePassword = async () => {
        if (dialogPassword) {
            if ((values.password.length > 0) && (editor.option.crypto.pku)) {
                setValues({ ...values, disabled: true, execute: true });
                if (!Arekey.has({nanoid: editor.value})) {
                    Arekey.creator({nanoid: editor.value, crypto: editor.option.crypto.pku});
                }
                let active = await Arekey.activate({nanoid: editor.value, passphrase: values.password});
                //console.log(Date.now() + ': active=' + active);
                //let k = await getPBKAESGCM(values.password, editor.option.crypto.pku);
                if (active) {//!k.fail
                    //dispatch(setMasterKey(k));
                    if (hybridPending) {
                        if (hybridPending.decrypt) {
                            //let data = await Arekey.decrypt({nanoid: editor.value, ...hybridPending.hybrid.option.crypto, ...hybridPending.hybrid.data.crypto});
                            //console.log(Date.now() + JSON.stringify(data));
                            dispatch(decryptHybrid({...hybridPending.hybrid}));
                        }
                        else {
                            dispatch(changeHybrid({...hybridPending.hybrid}));//, master: k
                        }
                    }
                    // else {// Test random key encrypt/decrypt OK.
                    //     let ct = await encryptAESGCMChain(k, JSON.stringify(hybrid.data));
                    //     let pt = await decryptAESGCMChain(k, ct.eku, ct.ctu);
                    //     console.log(JSON.stringify(ct));
                    //     console.log(pt);
                    // }
                    resetValues();
                    dispatch(openDialogPassword(false));
                }
                else {
                    setValues({ ...values, verifyerror: 'Password incorrect.' });
                }
            }
            else {
                setValues({ ...values, verifyerror: 'Password verify error.' });
            }
        }
    };

    const [values, setValues] = useState({
        disabled: false,
        password: '',
        showPassword: false,
        verifyerror: '',
        execute: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({
          ...values,
          showPassword: !values.showPassword,
        });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    if (editor?.option?.crypto) {
        return (
            <Dialog open={dialogPassword}>
                <DialogTitle>Password</DialogTitle>
                <DialogContent dividers>
                    <DialogContentText>
                        Please input password to protected editor master key.
                    </DialogContentText>
                    <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                    <FormControl focused required sx={{m: 1, width: '64ch'}} variant="outlined">
                    {editor?.option?.crypto ? <InputLabel htmlFor="outlined-adornment-password-pbk">Password to protect master key</InputLabel> : <InputLabel htmlFor="outlined-adornment-password-pbk">Password to protect master key</InputLabel>}
                        <OutlinedInput
                            id="outlined-adornment-password-pbk"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.password}
                            onChange={handleChange('password')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge='end'>
                                        {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label='Password to protect master key'
                        />
                        <FormHelperText error={values.verifyerror !== ''}>{values.verifyerror}</FormHelperText>
                    </FormControl>
                    </Box>
                </DialogContent>
                <DialogActions>
                    {editor?.option?.crypto && <Button disabled={values.execute} autoFocus onClick={handleChangePassword}>
                        OK
                    </Button>}
                </DialogActions>
            </Dialog>
        );
    
    }

}
