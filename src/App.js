import React, { useMemo } from 'react';
import "allotment/dist/style.css";
import './App.css';
import { createTheme, ThemeProvider} from '@mui/material';
import { Box, IconButton, Paper } from "@mui/material";
import { Brightness4, Brightness7, Folder, Search, Settings, Sync } from "@mui/icons-material";//ModeEdit, 
import AppHeader from './components/AppHeader';
import TEditor from './components/AppTiptap';
import AppTreeView from './components/AppTreeView';
import AppStatus from './components/AppStatus';
import { actionToggled } from './features/activityBar/activityBarSlice';
import { useDispatch, useSelector } from 'react-redux';
import { Allotment } from "allotment";
import AppDialog from './components/AppDialog';
import SyncSettings from './components/SyncSettings';
import SyncMain from './components/SyncMain';

function App() {
  const dispatch = useDispatch();
  const activity = useSelector(state => state?.activityBar?.activity);

  const editorToggle = () => {
    dispatch(actionToggled('editor'));
  };

  const syncToggle = () => {
    dispatch(actionToggled('sync'));
  };

  const [mode, setMode] = React.useState('light');

  const toggleThemeMode = () => {
    setMode (mode === 'light' ? 'dark' : 'light');
  };

  const theme = useMemo(
    () => createTheme({
      palette: {
        mode
      }
    }),
    [mode]
  );

  return (
    <ThemeProvider theme={theme}>
      <Paper sx={{ height: '100vh', display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
        <Box sx={{ display: 'flex', boxSizing: 'content-box' }}>
          <AppHeader />
        </Box>
        <Box sx={{ display: 'flex', flexGrow: 1, boxSizing: 'border-box', position: 'relative' }}>
          <Box sx={{display: 'flex', flexDirection: 'column', flexShrink: 0, justifyContent: 'space-between'}}>
            <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'column', padding: '4px 0'}}>
              <IconButton onClick={editorToggle}><Folder></Folder></IconButton>
              <IconButton disabled><Search></Search></IconButton>
              <IconButton onClick={syncToggle}>
                <Sync />
              </IconButton>
              <IconButton disabled><Settings></Settings></IconButton>
            </Box>
            <Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'column', padding: '4px 0'}}>
              <IconButton onClick={toggleThemeMode}>{mode === 'light' ? <Brightness4/>:<Brightness7/>}</IconButton>
            </Box>
          </Box>
          <Box sx={{flexGrow: 1, position: 'relative'}}>
            <Allotment separator={activity}>
              <Allotment.Pane
                minSize={activity ? 190 : 0}
                maxSize={activity ? 400 : 0}
                visible={activity}>
                <AppTreeView />
                <SyncSettings />

              </Allotment.Pane>
              <Allotment.Pane>
                <TEditor />
                <SyncMain />
              </Allotment.Pane>
            </Allotment>
          </Box>
        </Box>
        <AppStatus />
        <AppDialog />
      </Paper>
    </ThemeProvider>
  );

}

export default App;
