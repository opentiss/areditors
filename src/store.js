import { configureStore } from "@reduxjs/toolkit";
import optionsReducer from "./features/options/optionsSlice";
import actionStateSlice from './features/action/actionStateSlice'
import categoriesReducer from "./features/categories/categoriesSlice"
import contentStateReducer from "./features/content/contentStateSlice"
import activityBarReducer from "./features/activityBar/activityBarSlice";

export const store = configureStore({
    reducer: {
        activityBar: activityBarReducer,
        action: actionStateSlice,
        categories: categoriesReducer,
        contentState: contentStateReducer,
        options: optionsReducer,
    },
    // middleware: (getDefaultMiddleware) =>
    // getDefaultMiddleware({
    //   serializableCheck: {
    //     // Ignore these action types
    //     ignoredActions: ['options/setMasterKey', 'contentState/changeHybrid'],
    //     // Ignore these field paths in all actions
    //     ignoredActionPaths: ['meta.arg.master.key', 'meta.arg.master.iv', 'payload.key', 'payload.iv'],
    //     // Ignore these paths in the state
    //     ignoredPaths: ['options.editor.key'],
    //   },
    // }),
});
