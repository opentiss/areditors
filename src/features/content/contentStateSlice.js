import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import Arekey from "../../models/Arekey";
import { encryptHybrid, fetchHybridCategory, updateCategoryMimetype, updateHybrid } from "../../models/db";

const contentStateAdapter = createEntityAdapter();

const initialState = contentStateAdapter.getInitialState({
    autoSave: false,
    contentChanged: false,
    hybridNanoid: undefined,
    hybrid: undefined,
    hybridPending: undefined,
    timeoutID: undefined,
});

export const fetchHybrid = createAsyncThunk(
    'contentState/fetchHybrid',
    async (nanoid) => {
        if (nanoid) {
            return await fetchHybridCategory(nanoid);
        }
    }
);

export const changeHybrid = createAsyncThunk(
    'contentState/changeHybrid',
    async (hybrid) => {
        if (hybrid.nanoid) {
            const {data, master, mimetypechange, option, ...others} = hybrid;
            let crypto, raw;
            if (mimetypechange) {
                if (mimetypechange.includes('json')) {
                    raw = JSON.stringify(data);
                } else {
                    raw = data;
                }
                await updateCategoryMimetype(hybrid.nanoid, mimetypechange);
            } else {
                if (hybrid.mimetype.includes('json')) {
                    raw = JSON.stringify(data);
                } else {
                    raw = data;
                }
            }
            if (option?.crypto) {
                crypto = await Arekey.encrypt({nanoid: hybrid.editor, plain: raw});
                console.log(JSON.stringify(crypto));
            }
            await updateHybrid(hybrid.nanoid, data, crypto, hybrid?.option || {});
            return {mimetype: mimetypechange, ...others};
            // if (mimetypechange) {
            //     if (mimetypechange.includes('json') && master) {
    
            //         await updateHybrid(hybrid.nanoid, JSON.stringify(hybrid.data), master, hybrid?.option || {});
            //     }
            //     else {
            //         await updateHybrid(hybrid.nanoid, hybrid.data, master, hybrid?.option || {});
            //     }
            //     await updateCategoryMimetype(hybrid.nanoid, mimetypechange);
            //     return {...others, editing: true, mimetype: mimetypechange};
            // }
            // else {
            //     if (hybrid.mimetype.includes('json') && master) {
            //         if (option?.crypto) {
            //             let crypto = await Arekey.encrypt({nanoid: hybrid.editor, plain: JSON.stringify(hybrid.data)});
            //             await updateHybrid(hybrid.nanoid, JSON.stringify(hybrid.data), crypto, hybrid?.option || {});
            //             console.log(JSON.stringify(crypto));
            //         } else {
            //             await updateHybrid(hybrid.nanoid, JSON.stringify(hybrid.data), master, hybrid?.option || {});
            //         }
            //     }
            //     else {
            //         await updateHybrid(hybrid.nanoid, hybrid.data, master, hybrid?.option || {});
            //     }
            //     return {...others, editing: true}
            // }
        }
    }
);

export const cryptoHybrid = createAsyncThunk(
    'contentState/cryptoHybrid',
    async (hybrid) => {
        if (hybrid.nanoid) {
            await encryptHybrid(hybrid.nanoid, hybrid.option);
            return hybrid;
        }
    }
);

export const decryptHybrid = createAsyncThunk(
    'contentState/decryptHybrid',
    async (hybrid) => {
        if (hybrid.nanoid) {
            let data = await Arekey.decrypt({nanoid: hybrid.editor, ...hybrid.option.crypto, ...hybrid.data.crypto});
            // const data = await decryptAESGCMChain(master, others.option.crypto.eku, others.data.crypto.ctu);//encryptHybrid(hybrid.nanoid, hybrid.option);
            // if (data === '[object Object]') {
            //     console.log('[object Object]' + JSON.stringify(others));//return {...others, data: data};
            // }
            return {data: JSON.parse(data)};
        }
    }
);


const contentStateSlice = createSlice({
    name: 'contentState',
    initialState,
    reducers: {
        autoSaveToggled(state) {
            state.autoSave = !state.autoSave;
        },
        contentChangedToggled(state) {
            state.contentChanged = !state.contentChanged;
        },
        setHybridPending(state, action) {
            if (action.payload) {
                const {decrypt, mimetypechange, ...hybrid} = action.payload;
                if (mimetypechange && hybrid) {
                    state.hybridPending = {mimetypechange: mimetypechange, hybrid: hybrid};
                }
                else if (decrypt && hybrid) {
                    state.hybridPending = {hybrid: hybrid, decrypt: decrypt};
                }
                else if (hybrid) {
                    state.hybridPending = {hybrid: hybrid};
                }
            }
            else {
                state.hybridPending = undefined;
            }
        },
        timeoutIDUpdate(state, action) {
            state.timeoutID = action.payload;
        },
    },
    extraReducers(builder) {
        builder.addCase(fetchHybrid.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            hybrid: {...action.payload, loading: true},
        })
        );
        builder.addCase(changeHybrid.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            hybrid: {...state.hybrid, ...action.payload, loading: false},
            hybridPending: undefined,
            autoSave: false,
            contentChanged: false,
            timeoutID: undefined,
        }));
        builder.addCase(cryptoHybrid.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            hybrid: {...state.hybrid, option: action.payload.option},
            autoSave: true,
            contentChanged: true,
            timeoutID: undefined,
        }));
        builder.addCase(decryptHybrid.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            hybrid: {...state.hybrid, data: action.payload.data},
            hybridPending: undefined,
        }));

    }
//
});

export const {
    autoSaveToggled,
    contentChangedToggled,
    hybridMimetypeUpdate,
    setHybridPending,
    timeoutIDUpdate,
} = contentStateSlice.actions;

export default contentStateSlice.reducer;
