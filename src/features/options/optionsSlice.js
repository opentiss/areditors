import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import { echo, addDiary, updateEditorOption, updateEditorTitle } from "../../models/db";

const optionsAdapter = createEntityAdapter();

const initialState = optionsAdapter.getInitialState({
    status: 'idle',
    error: null
});

export const fetchOptions = createAsyncThunk(
    'options/fetchOptions',
    async () => {
        //const dispatch = useDispatch();
        const options = await echo.options.toArray();
        for (let i = 0; i < options.length; i++) {
            if (options[i].key === 'editor') {
                const editor = await echo.editors.where({nanoid: options[i].value}).first(
                    value => value
                );
                options[i].title = editor.title;
                options[i].option = editor.option;
                await addDiary(options[i].value);
            }
        }
        const data = await options.reduce( (prev, current) => {
            const {key, ...others} = current;
            prev[current.key] = {...others};
            // if (current.key === 'editor') {
            //     const editor = await echo.editors.where({nanoid: current.value}).first(
            //         value => value
            //     );
            //     prev[current.key].title = editor.title;
            //     prev[current.key].option = editor.option;
            //     await addDiary(current.value);
            //     //store.dispatch(fetchCategories(current.value));
            //     //dispatch(fetchCategories(current.value));
            // }
            // else if (current.key === 'hybrid') {
            //     //store.dispatch(hybridNanoidUpdate(current.value));
            //     //store.dispatch(fetchHybrid(current.value));
            //     //dispatch(fetchHybrid(current.value));
            // }
            return prev;
        }, {});
        //const dispatch = useDispatch();
        //dispatch(fetchCategories(data?.editor?.value));
        //store.dispatch(fetchHybrid(data?.hybrid?.value));
        return data;
    }
)

export const changeEditorTitle = createAsyncThunk(
    'options/changeEditorTitle',
    async (editor) => {
        if (editor?.title) {
            if (await updateEditorTitle(editor.nanoid, editor.title)) {
                return editor.title;
            }
        }
    }
)

export const changeEditorOption = createAsyncThunk(
    'options/changeEditorOption',
    async (editor) => {
        if (editor?.option) {
            if (await updateEditorOption(editor.nanoid, editor.option)) {
                return editor.option;
            }
        }
    }
)

const optionsSlice = createSlice({
    name: 'options',
    initialState,
    reducers: {
        setHybrid(state, action) {
            if (state?.hybrid?.value !== action.payload) {
                state.hybrid.value = action.payload;
            }
        },
    },
    extraReducers(builder) {
        builder.addCase(fetchOptions.fulfilled,
            (state, action) => ({
            ...state,
            status: 'succeeded',
            ...action.payload,
        })
        );
        builder.addCase(changeEditorOption.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            editor: {...state.editor, option: action.payload}
        })
        );
        builder.addCase(changeEditorTitle.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            editor: {...state.editor, title: action.payload}
        })
        );
    }
});

export const {
    setHybrid,
} = optionsSlice.actions;

export default optionsSlice.reducer;
