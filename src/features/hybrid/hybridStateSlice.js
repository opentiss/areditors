import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import { echo } from "../../models/db";

const hybridAdapter = createEntityAdapter();

const initialState = hybridAdapter.getInitialState({
    status: 'idle',
    error: null
});

/*export const fetchHybrid = createAsyncThunk(
    'hybrid/fetchHybrid',
    async (nanoid) => {
        if (nanoid) {
            //const hybrids = await echo.hybrids.toArray();
            const hybrid = await echo.hybrids.where({nanoid: nanoid}).toArray();
            if (!hybrid.length) {
                await echo.hybrids.add({nanoid: nanoid});
            }
            return hybrid;//await echo.hybrid.where({nanoid: nanoid}).toArray();
        }
    }
)*/

const hybridSlice = createSlice({
    name: 'hybrid',
    initialState,
    reducers: {
        //
    },
    /*extraReducers(builder) {
        builder.addCase(fetchHybrid.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            entities: action.payload,
        })
        );
    }*/
})

export default hybridSlice.reducer;
