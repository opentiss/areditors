import { createEntityAdapter, createSlice } from "@reduxjs/toolkit";

const actionStateAdapter = createEntityAdapter();

const initialState = actionStateAdapter.getInitialState({
    actionImportExport: {dialog: false},
    actionPassword: {dialog: false},
    actionProfile: {dialog: false},
    actionSecurity: {dialog: false},
    actionSync: {
        signaling: false,
        signalingid: 0,
        group: '',
        connect: false,
        targetid: 0,
        sync: false,
        pssa: '',
    },
});

const actionStateSlice = createSlice({
    name: 'action',
    initialState,
    reducers: {
        openDialogImportExport(state, action) {
            state.actionImportExport.dialog = action.payload;
        },
        openDialogPassword(state, action) {
            state.actionPassword.dialog = action.payload;
        },
        openDialogProfile(state, action) {
            state.actionProfile.dialog = action.payload;
        },
        openDialogSecurity(state, action) {
            state.actionSecurity.dialog = action.payload;
        },
        setSignaling(state, action) {
            state.actionSync.signaling = action.payload;
        },
        setSignalingid(state, action) {
            state.actionSync.signalingid = action.payload;
        },
        setConnect(state, action) {
            state.actionSync.connect = action.payload;
        },
        setTargetid(state, action) {
            state.actionSync.targetid = action.payload;
        },
        setSync(state, action) {
            state.actionSync.sync = action.payload;
        },
        setPssa(state, action) {
            state.actionSync.pssa = action.payload;
        },
    }
});

export const {
    openDialogImportExport,
    openDialogPassword,
    openDialogProfile,
    openDialogSecurity,
    setSignaling,
    setSignalingid,
    setConnect,
    setTargetid,
    setSync,
    setPssa,
} = actionStateSlice.actions;

export default actionStateSlice.reducer;
