import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    activity: false,
    actions: {
        editor: true,
        sync: false
    },
};

const activityBarSlice = createSlice({
    name: 'activityBar',
    initialState,
    reducers: {
        activityToggled(state) {
            state.activity = !state.activity;
        },
        actionToggled(state, action) {
            const actions = ['editor', 'sync'];
            if (state.actions[action.payload]) {
                state.activity = !state.activity;
            }
            else {
                const actionsToggled = {};
                actions.forEach(
                    (i) => i === action.payload ? actionsToggled[i] = true : actionsToggled[i] = false
                );
                state.actions = actionsToggled;
            }
        },
    }
});

export const {
    activityToggled,
    actionToggled
} = activityBarSlice.actions;

export default activityBarSlice.reducer;
