import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import { fetchCategoriesArray, addCategory, updateCategoryTitle } from "../../models/db";

const categoriesAdapter = createEntityAdapter();

const initialState = categoriesAdapter.getInitialState({
    status: 'idle',
    error: null
});

export const fetchCategories = createAsyncThunk(
    'categories/fetchCategories',
    async (editorNanoid) => {
        if (editorNanoid) {
            const categories = await fetchCategoriesArray(editorNanoid);
            const data = categories.filter( category => !category.parent );
            const keys = ['key', 'tag', 'type', 'title'];
            // ONLY for update tag!!!
            // categories.forEach(
            //     async (c) => {
            //         if (c.tag) {
            //             console.log(c.tag);
            //             //console.log(c.tag.padStart(c.tag.length + 2, '0'));
            //             // await echo.categories.where({nanoid: c.nanoid}).modify({tag: c.tag.padStart(c.tag.length + 2, '0')}).then(
            //             //     (v) => console.log(JSON.stringify(v))
            //             // );
            //         }
            //     }
            // );
            let journal = data.find((v) => v.key === 'journal');
            //journal.journal = 'journal';
            const journalChildren = (e) => {
                if (e.children) {
                    e.journal = 'journal';
                    e.children.forEach(
                        c => {
                            journalChildren(c);
                        }
                    );
                }
            };
            // TODO: move to utils.
            const fetchChildren = (e, category) => {
                category.forEach(
                    (c) => {
                        if (e.nanoid === c.parent) {
                            e.children ??= [];
                            if (!e.children.find( p => p.nanoid === c.nanoid )) {
                                e.children.push(c);
                            }
                        }
                    }
                );
                if (e.children) {
                    e.children.sort((a, b) => sortByKeys(a, b, keys));//sortArrayWithChildren(e.children, ['key', 'tag', 'title']);
                    e.children.forEach(
                        (c) => fetchChildren(c, categories)
                    );
                }

            };
            // TODO: move to utils.
            // sort by keys
            const sortByKeys = (a, b, p) => {
                const s = [];
                p.forEach(
                    (k) => {
                        if (!s.length) {
                            if (Object.hasOwn(a, k) && Object.hasOwn(b, k)) {
                                if (a[k] > b[k]) {
                                    s.push(1);
                                }
                                else if (a[k] < b[k]) {
                                    s.push(-1);
                                }
                                //else {
                                //    s.push(0);
                                //}
                            }
                            else if (Object.hasOwn(a, k)) {
                                s.push(-1);
                            }
                            else if (Object.hasOwn(b, k)) {
                                s.push(1);
                            }
    
                        }
                    }
                );
                if (s.length) {
                    return s[0];
                }
                else {
                    return 0;
                }
            };
            if (data?.length) {
                data.forEach(
                    (d) => {
                        fetchChildren(d, categories);
                        // if (d.children) {
                        //     d.children.forEach(
                        //         (c) => findChildren(c, categories)
                        //     );
                        // }
                        // categories.forEach(
                        //     (c) => {
                        //         if (d.nanoid === c.parent) {
                        //             d.children ??= [];
                        //             if (!d.children.find( p => p.nanoid === c.nanoid )) {
                        //                 d.children.push(c);
                        //             }
                        //         }
                        //     }
                        // );
                    }
                );
            }
            data.sort((a, b) => sortByKeys(a, b, keys));
            journalChildren(journal);
            //sortArrayWithChildren(data, ['key', 'tag', 'title']);
            /*categories.reduce((prev, current) => {
                prev[current.nanoid] = {id: current.id, title: current.title, flag: current.flag};
                if (current.key) {
                    prev[current.nanoid].key = current.key;
                }
                if (current.flag) {
                    prev[current.nanoid].flag = current.flag;
                }
                return prev;
            }, {});*/
            /*const findCategory = (e, category) => {
                if (e.nanoid === category.nanoid) {
                    return true;
                }
                else if (e.nanoid === category.parent) {
                    e.children ??= [];
                    if (!e.children.find( c => c.nanoid === category.nanoid )) {
                        e.children.push(category);
                    }
                    return true;
                }
                else if (e.children) {
                    return e.children.find( c => findCategory(c, category) );
                    //if (!e.children.find( c => c.nanoid === category.nanoid )) {
                    //    e.children.push(category);
                    //}
                }

            };
            let complete = categories.every( category => {
                let inData = data.find( e => findCategory(e, category) );
                return inData;
            });
            console.log('complete=' + complete + JSON.stringify(data));
            if (!complete) {
                complete = categories.every( category => {
                    let inData = data.find( e => findCategory(e, category) );
                    return inData;
                });
                console.log('complete=' + complete + JSON.stringify(data));
            }
            //while (!complete) {
            //    complete = categories.every();
            //}*/
            return data;//await echo.categories.where({editor: editorNanoid}).toArray();
        }
    }
);

export const createCategory = createAsyncThunk(
    'categories/createCategory',
    async (category) => {
        if (category) {
            const data = await addCategory(category);
            return data;
         }
    }
);

export const changeCategoryTitle = createAsyncThunk(
    'categories/changeCategoryTitle',
    async (category) => {
        if (category?.nanoid) {
            const data = await updateCategoryTitle(category.nanoid, category.title);
            return data;
         }
    }
);

const categoriesSlice = createSlice({
    name: 'categories',
    initialState,
    reducers: {
        //
    },
    extraReducers: (builder) => {
        builder.addCase(fetchCategories.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
            entities: action.payload,
        })
        );
        builder.addCase(createCategory.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
        })
        );
        builder.addCase(changeCategoryTitle.fulfilled, (state, action) => ({
            ...state,
            status: 'succeeded',
        })
        );
    }
})

export default categoriesSlice.reducer;
